// Nav

(function() {


    $('.sticky-nav__button').on('click', function(e){
        e.preventDefault();
        $('.sticky-nav').toggleClass('nav_open');
    });


    $('.sticky-form__button').on('click', function(e){
        e.preventDefault();
        $('.sticky-form').toggleClass('form_open');
    });


}());


$('body').click(function (event) {

    if ($(event.target).closest(".sticky-nav").length === 0) {
        $(".sticky-nav").removeClass('nav_open');
    }

    if ($(event.target).closest(".sticky-form").length === 0) {
        $(".sticky-form").removeClass('form_open');
    }
});

$(function($){
    $h = $('.content').offset().top;

    $(window).scroll(function(){
        // Если прокрутили скролл ниже макушки блока, включаем фиксацию

        if ( $(window).scrollTop() > $h) {
            $('.sticky-nav').addClass('nav_visible');
            $('.sticky-form').addClass('nav_visible');
            $('.btn-up').addClass('btn-up_visible');
            $('.btn-hide').addClass('btn-hide_visible');
            $('.btn-chat').addClass('btn-chat_visible');
        }else{
            //Иначе возвращаем всё назад. Тут вы вносите свои данные
            $('.sticky-nav').removeClass('nav_visible');
            $('.sticky-form').removeClass('nav_visible');
            $('.btn-up').removeClass('btn-up_visible');
            $('.btn-hide').removeClass('btn-hide_visible');
            $('.btn-chat').removeClass('btn-chat_visible');
        }
    });
});

// Scroll


$('.sticky-nav__content a, .service__nav a').click(function(){
   $('.sticky-nav').removeClass('nav_open');
    var str=$(this).attr('href');
    $.scrollTo(str, {duration: 500});
    return false;
});

$('.toc__nav a').click(function(){
    var str=$(this).attr('href');
    $.scrollTo(str, {duration: 500});
    return false;
});

$('.btn_scroll').click(function(){
    var str=$(this).attr('href');
    $.scrollTo(str, {duration: 500});
    return false;
});


$('.service__link').click(function(e){
    e.preventDefault();
    $('.service__nav').slideToggle('fast');
});