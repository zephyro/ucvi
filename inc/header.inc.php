<header class="с-site-header header">
    <div class="top-line">
        <div class="container">
            <div class="logo">
                <a href="/">
                    <img class="lazy loaded img_fluid" data-src="images/logo.svg" alt="ВЫСШАЯ ИНСТАНЦИЯ" src="images/logo.svg" data-was-processed="true">
                </a>
            </div>
            <div class="slogan">Земельный юрист №1 <br/>в Санкт-Петербурге</div>

            <div class="mobile-nav-wrap">
                <a href="#" class="main-nav-opener"><span></span></a>
                <div class="mobile-nav-drop">
                    <ul class="mobile-nav">
                        <li class="mobile-nav__item"><a href="#" class="mobile-nav__link" data-main-nav="2">Услуги</a>
                        </li>
                        <li class="mobile-nav__item"><a href="#mass-modal" class="mobile-nav__link fancybox">Мы
                                в СМИ</a></li>
                        <li class="mobile-nav__item"><a href="https://xn--b1am4b5a.xn--p1ai/contacts"
                                                        class="mobile-nav__link">Контакты</a></li>
                    </ul>

                    <div class="main-office">
                        <span class="main-office__sub-title">Центральный филиал в<br>Санкт-Петербурге</span>
                        <address class="main-office__address">Лиговский пр. дом 21</address>
                        <div class="office-slide-holder">
                            <a href="#"
                               class="offices-box__content-opener js-office-open-close"><span>Отделения</span></a>
                            <div class="slide js-slide-hidden" style="display: none;">
                                <div class="offices-box__content">
                                    <div class="offices-box__offices-zones">

                                    </div>
                                    <div class="offices-box__offices-content">
                                        <span class="offices-box__content-title">Отделения</span>
                                        <ul class="offices-list">
                                            <li>
                                                <div class="offices-list__icon">
                                                    <img class="lazy loaded"
                                                         data-src="images/icon-underground.png" alt=""
                                                         width="15" height="11"
                                                         src="images/icon-underground.png"
                                                         data-was-processed="true">
                                                </div>
                                                <div class="offices-list__text"><b>Комендантский проспект</b>,
                                                    ул.
                                                    Гаккелевская, 21. БЦ Балтийский Деловой Центр (Ресо)
                                                </div>
                                            </li>
                                            <li>
                                                <div class="offices-list__icon">
                                                    <img class="lazy loaded"
                                                         data-src="images/icon-underground.png" alt=""
                                                         width="15" height="11"
                                                         src="images/icon-underground.png"
                                                         data-was-processed="true">
                                                </div>
                                                <div class="offices-list__text"><b>Ладожская</b>, Пл. Карла
                                                    Фаберже д.
                                                    8. БЦ Русские Самоцветы
                                                </div>
                                            </li>
                                            <li>
                                                <div class="offices-list__icon">
                                                    <img class="lazy loaded"
                                                         data-src="images/icon-underground.png" alt=""
                                                         width="15" height="11"
                                                         src="images/icon-underground.png"
                                                         data-was-processed="true">
                                                </div>
                                                <div class="offices-list__text"><b>Петроградская</b>, ул.
                                                    Профессора
                                                    Попова 37 лит Щ. БЦ Сенатор
                                                </div>
                                            </li>
                                            <li>
                                                <div class="offices-list__icon">
                                                    <img class="lazy loaded"
                                                         data-src="images/icon-underground.png" alt=""
                                                         width="15" height="11"
                                                         src="images/icon-underground.png"
                                                         data-was-processed="true">
                                                </div>
                                                <div class="offices-list__text"><b>Площадь Александра
                                                        Невского</b>, ул.
                                                    Херсонская д. 12-14. БЦ Renaissance Pravda
                                                </div>
                                            </li>
                                            <li>
                                                <div class="offices-list__icon">
                                                    <img class="lazy loaded"
                                                         data-src="images/icon-underground.png" alt=""
                                                         width="15" height="11"
                                                         src="images/icon-underground.png"
                                                         data-was-processed="true">
                                                </div>
                                                <div class="offices-list__text"><b>Площадь Ленина</b>,
                                                    Пискарёвский
                                                    просп., 2. БЦ Бенуа
                                                </div>
                                            </li>
                                            <li>
                                                <div class="offices-list__icon">
                                                    <img class="lazy loaded"
                                                         data-src="images/icon-underground.png" alt=""
                                                         width="15" height="11"
                                                         src="images/icon-underground.png"
                                                         data-was-processed="true">
                                                </div>
                                                <div class="offices-list__text"><b>Пролетарская</b>, пр.
                                                    Обуховской
                                                    обороны 112, литера И, 3. БЦ Вант
                                                </div>
                                            </li>
                                            <li>
                                                <div class="offices-list__icon">
                                                    <img class="lazy loaded"
                                                         data-src="images/icon-underground.png" alt=""
                                                         width="15" height="11"
                                                         src="images/icon-underground.png"
                                                         data-was-processed="true">
                                                </div>
                                                <div class="offices-list__text"><b>Сенная Площадь</b>, наб. р.
                                                    Мойки 58,
                                                    литера А. БЦ Мариинский
                                                </div>
                                            </li>
                                            <li>
                                                <div class="offices-list__icon">
                                                    <img class="lazy loaded"
                                                         data-src="images/icon-underground.png" alt=""
                                                         width="15" height="11"
                                                         src="images/icon-underground.png"
                                                         data-was-processed="true">
                                                </div>
                                                <div class="offices-list__text"><b>Технологический Институт</b>,
                                                    10-я
                                                    Красноармейская, 22. БЦ Kellermann Center
                                                </div>
                                            </li>
                                            <li>
                                                <div class="offices-list__icon">
                                                    <img class="lazy loaded"
                                                         data-src="images/icon-underground.png" alt=""
                                                         width="15" height="11"
                                                         src="images/icon-underground.png"
                                                         data-was-processed="true">
                                                </div>
                                                <div class="offices-list__text"><b>Черная Речка</b>, Аптекарская
                                                    наб.,
                                                    д. 20. БЦ Авеню
                                                </div>
                                            </li>
                                            <li>
                                                <div class="offices-list__icon">
                                                    <img class="lazy loaded"
                                                         data-src="images/icon-underground.png" alt=""
                                                         width="15" height="11"
                                                         src="images/icon-underground.png"
                                                         data-was-processed="true">
                                                </div>
                                                <div class="offices-list__text"><b>Площадь Восстания</b>,
                                                    Лиговский
                                                    проспект 21. БЦ Сенатор
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="main-office__schedule">
                            <span class="main-office__schedule-label">Режим работы офиса:</span>
                            <span class="main-office__schedule-time">Пн. - Пт.: </span>
                            <span class="main-office__schedule-time">Суббота: </span>
                        </div>
                        <span class="main-office__schedule-ordering">Заключение договоров:<span>24/7</span></span>
                        <hr>

                        <p>
                            Вы можете <a href="#" class="copy_contact">скопировать наши контакты в буфер
                                обмена</a>.
                        </p>
                        <div class="copy_message">Контакты скопированы в буфер обмена.</div>
                    </div>
                </div>

                <div class="js-mobile-navigation-drop mobile-nav-first" data-navigation-id="1">
                    <a href="#" class="btn-back">Специализация</a>
                    <ul class="mobile-nav-first__list">
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/specializations/%D0%97%D0%B5%D0%BC%D0%B5%D0%BB%D1%8C%D0%BD%D0%BE%D0%B5%20%D0%BF%D1%80%D0%B0%D0%B2%D0%BE">
                                <span class="title">Земельное <em>право</em></span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/specializations/%D0%9C%D0%B5%D0%B6%D0%B5%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5">
                                <span class="title">Межевание</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/specializations/%D0%9F%D1%80%D0%BE%D0%B1%D0%BB%D0%B5%D0%BC%D1%8B%20%D1%81%20%D0%BA%D1%80%D0%B5%D0%B4%D0%B8%D1%82%D0%B0%D0%BC%D0%B8">
                                                <span class="title">
                                                    Проблемы с кредитами
                                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/specializations/%D0%9D%D0%B5%D0%B4%D0%B2%D0%B8%D0%B6%D0%B8%D0%BC%D0%BE%D1%81%D1%82%D1%8C">
                                <span class="title">Недвижимость</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/specializations/%D0%96%D0%B8%D0%BB%D0%B8%D1%89%D0%BD%D0%BE%D0%B5%20%D0%BF%D1%80%D0%B0%D0%B2%D0%BE">
                                <span class="title">Жилищное <em>право</em></span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/specializations/%D0%94%D0%94%D0%A3%20%D0%B8%20%D0%96%D0%A1%D0%9A">
                                <span class="title">ДДУ и ЖСК</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/specializations/%D0%9D%D0%B0%D1%81%D0%BB%D0%B5%D0%B4%D1%81%D1%82%D0%B2%D0%BE">
                                <span class="title">Наследство</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/specializations/%D0%A2%D1%80%D1%83%D0%B4%D0%BE%D0%B2%D0%BE%D0%B5%20%D0%BF%D1%80%D0%B0%D0%B2%D0%BE">
                                <span class="title">Трудовое <em>право</em></span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/specializations/%D0%A1%D1%82%D1%80%D0%BE%D0%B8%D1%82%D0%B5%D0%BB%D1%8C%D1%81%D1%82%D0%B2%D0%BE">
                                <span class="title">Строительство</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/specializations/%D0%90%D1%80%D0%B1%D0%B8%D1%82%D1%80%D0%B0%D0%B6">
                                <span class="title">Арбитраж</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/specializations/%D0%92%D0%B7%D1%8B%D1%81%D0%BA%D0%B0%D0%BD%D0%B8%D0%B5">
                                <span class="title">Взыскание</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/specializations/%D0%90%D0%B2%D1%82%D0%BE%D0%BC%D0%BE%D0%B1%D0%B8%D0%BB%D1%8C%D0%BD%D0%BE%D0%B5%20%D0%BF%D1%80%D0%B0%D0%B2%D0%BE">
                                <span class="title">Автомобильное <em>право</em></span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/specializations/%D0%9F%D0%B5%D0%BD%D1%81%D0%B8%D0%BE%D0%BD%D0%BD%D0%BE%D0%B5%20%D0%BF%D1%80%D0%B0%D0%B2%D0%BE">
                                <span class="title">Пенсионное <em>право</em></span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/specializations/%D0%90%D0%B2%D1%82%D0%BE%D1%80%D1%81%D0%BA%D0%BE%D0%B5%20%D0%BF%D1%80%D0%B0%D0%B2%D0%BE">
                                <span class="title">Авторское <em>право</em></span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/specializations/%D0%A1%D0%B5%D0%BC%D0%B5%D0%B9%D0%BD%D0%BE%D0%B5%20%D0%BF%D1%80%D0%B0%D0%B2%D0%BE">
                                <span class="title">Семейное <em>право</em></span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/specializations/%D0%9C%D0%B8%D0%B3%D1%80%D0%B0%D1%86%D0%B8%D0%BE%D0%BD%D0%BD%D0%BE%D0%B5%20%D0%BF%D1%80%D0%B0%D0%B2%D0%BE">
                                <span class="title">Миграционное <em>право</em></span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/specializations/%D0%9C%D0%B5%D0%B4%D0%B8%D1%86%D0%B8%D0%BD%D1%81%D0%BA%D0%BE%D0%B5%20%D0%BF%D1%80%D0%B0%D0%B2%D0%BE">
                                <span class="title">Медицинское <em>право</em></span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/specializations/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5">
                                                <span class="title">Лицензирование
                                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/specializations/%D0%A3%D0%B3%D0%BE%D0%BB%D0%BE%D0%B2%D0%BD%D0%BE%D0%B5%20%D0%BF%D1%80%D0%B0%D0%B2%D0%BE">
                                <span class="title">Уголовное <em>право</em></span>
                            </a>
                        </li>
                    </ul>
                    <div class="mobile-nav-footer">
                        <div class="contact-info">
                            <span class="contact-info__question">Не нашли нужную услугу?</span>
                            <p>Оставьте номер телефона.<br>Наш специалист проконсультирует вас бесплатно!</p>
                        </div>
                        <div class="callback-form">
                            <form method="post" class="c-feedback-form js-feedback-form"
                                  data-parsley-validate="" novalidate="">
                                <div class="callback-form__row">
                                    <input type="button" class="js-feedback-form__submit submit"
                                           value="Отправить">
                                    <div class="callback-form__text-wrap">
                                        <input type="text" class="js-feedback-form__phone-input text phone-mask"
                                               placeholder="Ваш телефон" data-parsley-required=""
                                               data-parsley-phone="" im-insert="true">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="js-mobile-navigation-drop mobile-nav-first" data-navigation-id="2">
                    <a href="#" class="btn-back">Услуги</a>
                    <ul class="mobile-nav-first__list">
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/services/%D0%9A%D0%BE%D0%BD%D1%81%D1%83%D0%BB%D1%8C%D1%82%D0%B0%D1%86%D0%B8%D1%8F">
                                <span class="title">Консультация</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/services/%D0%A1%D1%83%D0%B4">
                                <span class="title">Суд</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/services/%D0%AE%D1%80%D0%B8%D1%81%D1%82%D1%8B">
                                <span class="title">Юристы</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/services/%D0%AE%D1%80%D0%B8%D1%81%D1%82%D1%8B">
                                <span class="title">Юристы</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/services/%D0%AE%D1%80%D0%B8%D0%B4%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F%20%D0%BF%D0%BE%D0%BC%D0%BE%D1%89%D1%8C">
                                <span class="title">Юридическая помощь</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-fancybox="" data-type="ajax"
                               data-src="https://xn--b1am4b5a.xn--p1ai/site/page-links/services/%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D0%BB%D1%8C%D0%BD%D0%BE%D0%B5%20%D1%81%D0%BE%D0%BF%D1%80%D0%BE%D0%B2%D0%BE%D0%B6%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5">
                                <span class="title">Документальное сопровождение</span>
                            </a>
                        </li>
                    </ul>
                    <div class="mobile-nav-footer">
                        <div class="contact-info">
                            <span class="contact-info__question">Не нашли нужную услугу?</span>
                            <p>Оставьте номер телефона.<br>Наш специалист проконсультирует вас бесплатно!</p>
                        </div>
                        <div class="callback-form">
                            <form method="post" class="c-feedback-form js-feedback-form"
                                  data-parsley-validate="" novalidate="">
                                <div class="callback-form__row">
                                    <input type="button" class="js-feedback-form__submit submit"
                                           value="Отправить">
                                    <div class="callback-form__text-wrap">
                                        <input type="text" class="js-feedback-form__phone-input text phone-mask"
                                               placeholder="Ваш телефон" data-parsley-required=""
                                               data-parsley-phone="" im-insert="true">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Desktop Navigation Bar -->
            <div class="top-nav-wrap">
                <div class="main-drop">
                    <ul class="top-nav">
                        <li class="top-nav__item hide">
                            <a href="#" class="top-nav__link has-drop">
                                <span>Специализация</span>
                            </a>
                            <div class="top-nav-drop top-nav-drop--specializations">
                                <div class="first-level-holder">
                                    <ul class="first-level">
                                        <li>
                                            <a href="#" class=" active ">
                                                <span class="title" data-id="menu-specialization-item-0">Земельное <em>право</em></span>
                                                <span class="number">(63)</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                                                <span class="title" data-id="menu-specialization-item-1">Межевание</span>
                                                <span class="number">(55)</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                                                <span class="title" data-id="menu-specialization-item-2">Проблемы с кредитами</span>
                                                <span class="number">(55)</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                                                <span class="title" data-id="menu-specialization-item-3">Недвижимость</span>
                                                <span class="number">(80)</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                                                <span class="title" data-id="menu-specialization-item-4">Жилищное <em>право</em></span>
                                                <span class="number">(36)</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                                                <span class="title" data-id="menu-specialization-item-5">ДДУ и ЖСК</span>
                                                <span class="number">(97)</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                                                <span class="title" data-id="menu-specialization-item-6">Наследство</span>
                                                <span class="number">(47)</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                                                <span class="title" data-id="menu-specialization-item-7">Трудовое <em>право</em></span>
                                                <span class="number">(20)</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                                                <span class="title" data-id="menu-specialization-item-8">Строительство</span>
                                                <span class="number">(18)</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                                                <span class="title" data-id="menu-specialization-item-9">Арбитраж</span>
                                                <span class="number">(22)</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                                                <span class="title" data-id="menu-specialization-item-10">Взыскание</span>
                                                <span class="number">(36)</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                                                <span class="title" data-id="menu-specialization-item-11">Автомобильное <em>право</em></span>
                                                <span class="number">(18)</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                                                <span class="title" data-id="menu-specialization-item-12">Пенсионное <em>право</em></span>
                                                <span class="number">(3)</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                                                <span class="title" data-id="menu-specialization-item-13">Авторское <em>право</em></span>
                                                <span class="number">(6)</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                                                <span class="title" data-id="menu-specialization-item-14">Семейное <em>право</em></span>
                                                <span class="number">(33)</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                                                <span class="title" data-id="menu-specialization-item-15">Миграционное <em>право</em></span>
                                                <span class="number">(2)</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                                                <span class="title" data-id="menu-specialization-item-16">Медицинское <em>право</em></span>
                                                <span class="number">(4)</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                                                <span class="title" data-id="menu-specialization-item-17">Лицензирование</span>
                                                <span class="number">(5)</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                                                <span class="title" data-id="menu-specialization-item-18">Уголовное <em>право</em></span>
                                                <span class="number">(66)</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="second-level-holder">
                                    <div class="second-level-services">
                                        <div class="drop-header">
                                                    <span class="drop-header__title">
                                                        <span class="drop-header__title-icon">
                                                            <img class="lazy loaded" data-src="images/icon-service.png" alt="" src="images/icon-service.png" data-was-processed="true">
                                                        </span>
                                                        <span class="drop-header__title-text popup_service_title">Специализации</span>
                                                    </span>
                                            <div class="sort-search">
                                                <div class="easy-autocomplete" style="width: 578.969px;"><input
                                                        type="text" class="text js-search-service-input"
                                                        placeholder="Начните набирать название..."
                                                        id="eac-9114" autocomplete="off">
                                                    <div class="easy-autocomplete-container"
                                                         id="eac-container-eac-9114">
                                                        <ul></ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="menu-specialization-item-0">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/arenda-zemli-uchastka-izhs-snt-stroitelstvo">Аренда земли</a>
                                                </li>
                                                <li>
                                                    <a href="/oformlenie-zemli-uchastka-doma-sobstvennost">Оформление земли</a>
                                                </li>
                                                <li>
                                                    <a href="/perevod-zemli-selskohozyajstvennaya-izhs-snt-lph">Перевод земли</a>
                                                </li>
                                                <li>
                                                    <a href="/yuridicheskaya-pomoshch-snt-zashchita-sadovodov">Вопросы по СНТ</a>
                                                </li>
                                                <li>
                                                    <a href="/yuridicheskoe-soprovozhdenie-dnp-zashchita-dachnikov">Вопросы по ДНП </a>
                                                </li>
                                                <li>
                                                    <a href="/dachnaya-amnistiya-zemelnogo-uchastka-oformit-sobstvennost-registraciya">Дачная амнистия земли</a>
                                                </li>
                                                <li>
                                                    <a href="/darenie-zemelnogo-uchastka-pomoshch-yurista">Дарение земельного участка</a>
                                                </li>
                                                <li>
                                                    <span class="font-weight-bold">Бесплатные услуги</span>

                                                    <ul class="mt-3 px-0">
                                                        <li>
                                                            <a href="/konsultaciya-zemelnym-voprosam-sporam-besplatno">Консультация по земельному праву</a>
                                                        </li>
                                                        <li>
                                                            <a href="/zemelnye-spory-granicy-sosedi-sud-yurist">Решение земельных споров</a>
                                                        </li>
                                                        <li>
                                                            <a href="/yuridicheskie-uslugi-zemelnym-voprosam-sporam">Юридическая помощь по земельным спорам</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="menu-specialization-item-1">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/mezhevanie-zemli-uchastka-spb-lo">Межевание</a>
                                                </li>
                                                <li>
                                                    <a href="/mezhevanie-zemli-uchastka-kadastrovye-raboty">Межевые работы</a>
                                                </li>
                                                <li>
                                                    <a href="/kadastrovye-raboty-zemelnom-uchastke-provedenie">Кадастровые работы</a>
                                                </li>
                                                <li>
                                                    <a href="/geodezicheskie-raboty-izyskaniya-semka">Геодезические работы</a>
                                                </li>
                                                <li>
                                                    <a href="/pereraspredelenie-zemelnyh-uchastkov-doley">Перераспределение земель</a>
                                                </li>
                                                <li>
                                                    <a href="/prirezka-zemli-zemelnogo-uchastka-oformlenie">Прирезка земли</a>
                                                </li>
                                                <li>
                                                    <a href="/nalozhenie-granic-zemelnogo-uchastka-pomoshch-yurista">Наложение границ земельного участка</a>
                                                </li>
                                                <li>
                                                    <span class="font-weight-bold">Бесплатные услуги</span>

                                                    <ul class="mt-3 px-0">
                                                        <li>
                                                            <a href="/mezhevye-spory-o-granicah-mezhdu-sosedyami">Решение межевых споров</a>
                                                        </li>
                                                        <li>
                                                            <a href="/prirezka-zemli-zemelnogo-uchastka-oformlenie">Прирезка земли</a>
                                                        </li>
                                                        <li>
                                                            <a href="/opredelenie-poryadka-polzovaniya-zemelnym-uchastkom">Определение порядка пользования ЗУ</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="menu-specialization-item-2">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/bankrotstvo-fizicheskih-lic-ip-konsultaciya">Банкротство физических лиц</a>
                                                </li>
                                                <li>
                                                    <a href="/problemy-s-kreditami-pomoshch-yurista">Проблемы с кредитами</a>
                                                </li>
                                                <li>
                                                    <a href="/restrukturizaciya-kredita-dolga-valyutnoj-ipoteki">Реструктуризация кредитов</a>
                                                </li>
                                                <li>
                                                    <a href="/refinansirovanie-kreditov-bankov-ipoteki-dolgov">Рефинансирование кредитов</a>
                                                </li>
                                                <li>
                                                    <a href="/sud-s-bankom-po-kreditu-ipoteke-yuristy-banki-spb-moskva">Спор с банком по кредиту</a>
                                                </li>
                                                <li>
                                                    <a href="/rastorzhenie-kreditnogo-dogovora-bankom-sud">Расторжение кредитного договора</a>
                                                </li>
                                                <li>
                                                    <a href="/pomoshch-kreditnogo-brokera-prosrochki">Получение кредита</a>
                                                </li>
                                                <li>
                                                    <a href="/pomoshch-vkladchikam-semejnogo-kapitala">Помощь вкладчикам КПК "Семейный капитал"</a>
                                                </li>
                                                <li>
                                                    <span class="font-weight-bold">Бесплатные услуги</span>

                                                    <ul class="mt-3 px-0">
                                                        <li>
                                                            <a href="/konsultaciya-kreditnym-voprosam-yuridicheskaya-besplatnaya">Консультация по кредитным вопросам</a>
                                                        </li>
                                                        <li>
                                                            <a href="/kreditnyj-finansovyj-ipotechnyj-konsultant">Помощь по вопросам кредитования</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vklyuchenie-isklyuchenie-reestr-kreditorov">Помощь по реестру кредиторов</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="menu-specialization-item-3">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/privatizaciya-zhilya-nedvizhimosti-spb-moskva">Приватизация
                                                        имущества</a>
                                                </li>
                                                <li>
                                                    <a href="/Oformlenie-sobstvennosti-doma-zemli-uchastka-nasledstva-sdelki-kvartiry">Оформление
                                                        в собственность</a>
                                                </li>
                                                <li>
                                                    <a href="/konsultaciya-pri-pokupke-nedvizhimos">Консультация
                                                        при покупке недвижимости</a>
                                                </li>
                                                <li>
                                                    <a href="/perevod-nedvizhimosti-spb-moskva">Перевод
                                                        недвижимости</a>
                                                </li>
                                                <li>
                                                    <a href="/yuridicheskoe-soprovozhdenie-sdelki-nedvizhimost">Сделки
                                                        с недвижимостью</a>
                                                </li>
                                                <li>
                                                    <a href="/luchshaya-stoimost-pereplanirovki-spb-pomoshch-yurista">Перепланировка
                                                        по лучшей стоимости</a>
                                                </li>
                                                <li>
                                                    <a href="/rekonstrukciya-doma-pomoshch-yurista">Реконструкция
                                                        дома</a>
                                                </li>
                                                <li>
                                                    <a href="/osparivanie-kadastrovoj-stoimosti-nedvizhimosti">Оспаривание
                                                        кадастровой стоимости</a>
                                                </li>
                                                <li>
                                                    <span class="font-weight-bold">Бесплатные услуги</span>

                                                    <ul class="mt-3 px-0">
                                                        <li>
                                                            <a href="/yurist-po-nedvizhimosti-konsultaciya">Консультация
                                                                юриста по недвижимости</a>
                                                        </li>
                                                        <li>
                                                            <a href="/yurist-po-privatizacii-kvartiry-komnaty-zemli-doma">Помощь
                                                                по приватизации жилья</a>
                                                        </li>
                                                        <li>
                                                            <a href="/ehkspertiza-proverka-dogovorov-podryada-kupli-prodazhi">Экспертиза
                                                                и проверка договора</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="menu-specialization-item-4">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/zhilishchnye-spory-s-sosedyami-zhsk-tszh-uk-advokaty-yuristy">Жилищные
                                                        споры</a>
                                                </li>
                                                <li>
                                                    <a href="/vyselit-iz-kvartiry-zhilogo-pomeshcheniya">Выселение
                                                        и защита от выселения</a>
                                                </li>
                                                <li>
                                                    <a href="/uluchshenie-zhilishchnyh-uslovij-uchet-ochered-yuridicheskaya-pomoshch">Улучшение
                                                        жилищных условий</a>
                                                </li>
                                                <li>
                                                    <a href="/zaliv-zatoplenie-kvartiry-ocenka-ushcherba-pomoshch-yurist">Затопление
                                                        квартиры</a>
                                                </li>
                                                <li>
                                                    <span class="font-weight-bold">Бесплатные услуги</span>

                                                    <ul class="mt-3 px-0">
                                                        <li>
                                                            <a href="/yuridicheskaya-konsultaciya-po-zhilishchnym-voprosam">Консультация
                                                                по жилищным вопросам</a>
                                                        </li>
                                                        <li>
                                                            <a href="/zhilishchnye-spory-s-sosedyami-zhsk-tszh-uk-advokaty-yuristy">Разрешение
                                                                жилищных споров</a>
                                                        </li>
                                                        <li>
                                                            <a href="/zhilishchnyj-vopros-bystroe-reshenie-konsultacii-besplatno-yuristy-advokaty">Решение
                                                                жилищного вопроса</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="menu-specialization-item-5">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/proverka-ddu-dogovor-dolevogo-uchastiya-zastrojshchika-dolevka">Проверка
                                                        ДДУ</a>
                                                </li>
                                                <li>
                                                    <a href="/vzyskanie-neustojki-ddu-peni-dolevka-214-fz-zastrojshchik-zhsk">Взыскание
                                                        неустойки по ДДУ</a>
                                                </li>
                                                <li>
                                                    <a href="/zaderzhka-sdachi-doma-pomoshch-yurista">Задержка
                                                        сдачи дома</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-ddu-dolevka-214-fz-prosrochka-dolevoe-stroitelstvo-sud">Споры
                                                        по ДДУ</a>
                                                </li>
                                                <li>
                                                    <a href="/sdelka-dolevka-ddu-214-fz-dolevoe-zastrojshchik-prosrochka-sroka-zhsk">Сопровождение
                                                        сделок с ДДУ</a>
                                                </li>
                                                <li>
                                                    <a href="/rastorzhenie-dogovora-zhsk-prekrashchenie-chlenstva-yurist-advokat">Расторжение
                                                        договора ЖСК</a>
                                                </li>
                                                <li>
                                                    <a href="/bankrotstvo-zastrojshchika-pri-dolevom-stroitelstve">Банкротство
                                                        застройщиков по ДДУ</a>
                                                </li>
                                                <li>
                                                    <span class="font-weight-bold">Бесплатные услуги</span>

                                                    <ul class="mt-3 px-0">
                                                        <li>
                                                            <a href="/konsultaciya-yurista-advokata-besplatno-ddu-dolyovka-214-fz#">Бесплатные
                                                                консультации по ДДУ</a>
                                                        </li>
                                                        <li>
                                                            <a href="/yuridicheskaya-pomoshch-dolevka-ddu-214-fz-zastrojshchik-zhsk">Юридическая
                                                                помощь по ДДУ</a>
                                                        </li>
                                                        <li>
                                                            <a href="/yuridicheskaya-pomoshch-dolshchikam-yurist">Юридическая
                                                                помощь обманутым дольщикам</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="menu-specialization-item-6">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/vosstanovlenie-srokov-nasledstva-sud-yurist-advokat">Восстановление
                                                        сроков наследства</a>
                                                </li>
                                                <li>
                                                    <a href="/oformlenie-nasledstva-zaveshchanie-dokumenty">Оформление
                                                        наследства</a>
                                                </li>
                                                <li>
                                                    <a href="/poluchenie-nasledstva-spb-moskva">Получение
                                                        наследства</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-nasledstvu-konsultacii-besplatno-spb-moskva">Споры
                                                        по наследству</a>
                                                </li>
                                                <li>
                                                    <a href="/yuridicheskaya-konsultaciya-nasledstvennye-voprosy-spory-yurist-advokat">Консультация
                                                        по наследственным вопросам</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-nasledstvo-prinyatie-oformlenie-vstuplenie">Юридические
                                                        услуги по наследству</a>
                                                </li>
                                                <li>
                                                    <span class="font-weight-bold">Бесплатные услуги</span>

                                                    <ul class="mt-3 px-0">
                                                        <li>
                                                            <a href="/konsultaciya-po-nasledstvennomu-voprosu-spb-moskva">Бесплатная
                                                                консультация по наследству</a>
                                                        </li>
                                                        <li>
                                                            <a href="/spory-po-nasledstvu-konsultacii-besplatno-spb-moskva">Разрешение
                                                                споров по наследству</a>
                                                        </li>
                                                        <li>
                                                            <a href="/oformlenie-nasledstva-zaveshchanie-dokumenty">Юридическая
                                                                помощь по оформлению наследства</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="menu-specialization-item-7">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/reshenie-trudovyh-sporov-sud-yurist-advokat">Трудовые
                                                        споры</a>
                                                </li>
                                                <li>
                                                    <a href="/vosstanovlenie-rabote-uvolennogo-sotrudnika-isk-zayavlenie-sud">Восстановление
                                                        на работе</a>
                                                </li>
                                                <li>
                                                    <a href="/nezakonnoe-uvolnenie-konsultacii-onlain-besplatno-spb-moskva">Незаконное
                                                        увольнение</a>
                                                </li>
                                                <li>
                                                    <a href="/zaderzhka-zarabotnoj-platy-rabotodatelem-kompensaciya">Задержка
                                                        заработной платы</a>
                                                </li>
                                                <li>
                                                    <a href="/vneplanovaya-proverka-organizacii-trudovoj-inspekcii">Проверка
                                                        трудовой инспекции</a>
                                                </li>
                                                <li>
                                                    <span class="font-weight-bold">Бесплатные услуги</span>

                                                    <ul class="mt-3 px-0">
                                                        <li>
                                                            <a href="/konsultaciya-yurista-advokata-po-trudovomu-pravu-besplatno">Консультация
                                                                по трудовому праву</a>
                                                        </li>
                                                        <li>
                                                            <a href="/uslugi-yurista-advokata-po-trudovym-voprosam">Услуги
                                                                по трудовому праву</a>
                                                        </li>
                                                        <li>
                                                            <a href="/yuridicheskaya-pomoshch-po-trudovym-voprosam">Помощь
                                                                по трудовым вопросам</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="menu-specialization-item-8">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/spory-v-stroitelstve-dolevoe-uchastie-zastrojshchik-konsultaciya">Споры
                                                        в строительстве</a>
                                                </li>
                                                <li>
                                                    <a href="/poluchenie-razreshenie-na-stroitelstvo">Получение
                                                        разрешения на строительство</a>
                                                </li>
                                                <li>
                                                    <a href="/tekhnicheskie-usloviya-podklyuchenie-poluchenie">Получение
                                                        технических условий на объект строительства</a>
                                                </li>
                                                <li>
                                                    <a href="/kompleksnoe-osvoenie-territorii-stroitelstva">Комплексное
                                                        освоение территории строительства</a>
                                                </li>
                                                <li>
                                                    <span class="font-weight-bold">Бесплатные услуги</span>

                                                    <ul class="mt-3 px-0">
                                                        <li>
                                                            <a href="/stroitelnyj-yurist-konsultacii">Консультация
                                                                юриста по строительству</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-dolevomu-stroitelstvu-dolevka-ddu-214-fz">Консультация
                                                                юриста по долевому строительству</a>
                                                        </li>
                                                        <li>
                                                            <a href="/besplatnyj-yurist-onlajn">Юридическая
                                                                консультация онлайн</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="menu-specialization-item-9">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/arbitrazhnye-spory">Арбитражные споры</a>
                                                </li>
                                                <li>
                                                    <a href="/arbitrazh-zashchita-sud">Арбитражная защита</a>
                                                </li>
                                                <li>
                                                    <a href="/ehkspertiza-arbitrazhnyj-process-naznachenie-vozrazheniya">Экспертиза
                                                        в арбитражном процессе</a>
                                                </li>
                                                <li>
                                                    <a href="/hodatajstvo-arbitrazhnyj-sud-sostavlenie-podacha-yurist">Ходатайство
                                                        в арбитражный суд</a>
                                                </li>
                                                <li>
                                                    <a href="/vedenie-arbitrazhnyh-del-predstavitelstvo-yurista">Ведение
                                                        арбитражных дел</a>
                                                </li>
                                                <li>
                                                    <a href="/arbitrazhnyj-sud">Арбитражный суд</a>
                                                </li>
                                                <li>
                                                    <span class="font-weight-bold">Бесплатные услуги</span>

                                                    <ul class="mt-3 px-0">
                                                        <li>
                                                            <a href="/konsultaciya-arbitrazhnye-dela-besplatno-onlajn-yurist-advokat-sud">Консультация
                                                                по арбитражным вопросам</a>
                                                        </li>
                                                        <li>
                                                            <a href="/konsultaciya-yurista-po-vzyskaniyu-dolgov">Консультация
                                                                по взысканию неустоек, алиментов</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="menu-specialization-item-10">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/vzyskanie-zaloga">Взыскание залога</a>
                                                </li>
                                                <li>
                                                    <a href="/vzyskanie-neustoek-shtrafov-peni-cherez-sud">Взыскание
                                                        всех видов неустоек</a>
                                                </li>
                                                <li>
                                                    <a href="/vzyskanie-ubytkov-upushchennoj-vygody-arbitrazh">Взыскание
                                                        убытков и упущенной выгоде</a>
                                                </li>
                                                <li>
                                                    <a href="/vzyskanie-ushcherba-materialnogo-moralnogo-yurist">Взыскание
                                                        ущерба</a>
                                                </li>
                                                <li>
                                                    <a href="/pomoshch-vkladchikam-bm-invest">Возврат вкладов из
                                                        БМ "Инвест"</a>
                                                </li>
                                                <li>
                                                    <a href="/nekachestvennyj-remont-kvartiry-pomoshch-yurista">Взыскание
                                                        компенсации за некачественный ремонт</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="menu-specialization-item-11">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/vzyskanie-ushcherba-po-dtp-moralnaya-kompensaciya">Взыскание
                                                        ущерба после ДТП</a>
                                                </li>
                                                <li>
                                                    <a href="/rastorzhenie-dogovora-avtokredita">Расторжение
                                                        договора автокредита</a>
                                                </li>
                                                <li>
                                                    <a href="/sud-neuplata-avtokredita-bank-vozvrat-strahovki-pomoshch-yurist-advokat">Автокредит.
                                                        Защита в суде</a>
                                                </li>
                                                <li>
                                                    <a href="/vozhdenie-v-netrezvom-vide-pomoshch-yurista">Вождение
                                                        в нетрезвом виде</a>
                                                </li>
                                                <li>
                                                    <a href="/dosrochnyj-vozvrat-voditel%27skih-prav-pomoshch-yurista">Возврат
                                                        водительских прав</a>
                                                </li>
                                                <li>
                                                    <a href="/vozvrashchenie-voditelskih-prav">Возврат
                                                        водительских прав в сложных случаях</a>
                                                </li>
                                                <li>
                                                    <span class="font-weight-bold">Бесплатные услуги</span>

                                                    <ul class="mt-3 px-0">
                                                        <li>
                                                            <a href="/avtomobilnyj-yurist-konsultaciya-besplatno">Консультация
                                                                автоюриста</a>
                                                        </li>
                                                        <li>
                                                            <a href="/avtomobilnyj-advokat">Консультация
                                                                автомобильного юриста</a>
                                                        </li>
                                                        <li>
                                                            <a href="/yuridicheskaya-pomoshch-dtp-spoy-gibdd-vozmeshchenie-ushcherba-sud">Юридическая
                                                                помощь по ДТП</a>
                                                        </li>
                                                        <li>
                                                            <a href="/konsultaciya-po-avtokreditu">Консультация
                                                                по автокредиту</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="menu-specialization-item-12">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/pensionnye-spory">Пенсионные споры</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="menu-specialization-item-13">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/narushenie-avtorskih-prav-st-146-advokaty-yuristy-spb-moskva">Нарушение
                                                        авторских прав</a>
                                                </li>
                                                <li>
                                                    <a href="/zashchita-patentnogo-prava-pomoshch-yurista">Защита
                                                        патентного права</a>
                                                </li>
                                                <li>
                                                    <a href="/zashchita-intellektualnoj-sobstvennosti-pomoshch-yurista">Защита
                                                        интеллектуальной собственности</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="menu-specialization-item-14">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/rastorzhenie-braka-pomoshch-yurista-2017">Расторжение
                                                        брака</a>
                                                </li>
                                                <li>
                                                    <a href="/razdel-imushchestva-suprugov-pomoshch-yurista">Раздел
                                                        имущества</a>
                                                </li>
                                                <li>
                                                    <a href="/priznanie-braka-nedejstvitelnym-pomoshch-yurista">Признание
                                                        брака недействительным</a>
                                                </li>
                                                <li>
                                                    <a href="/uslugi-po-alimentam-pomoshch-yurista">Услуги по
                                                        алиментам</a>
                                                </li>
                                                <li>
                                                    <a href="/alimenty-spory-pomoshch-yurista">Споры по
                                                        алиментам</a>
                                                </li>
                                                <li>
                                                    <a href="/ustanovlenie-otcovstva-pomoshch-yurista">Установление
                                                        отцовства</a>
                                                </li>
                                                <li>
                                                    <a href="/osparivanie-otcovstva-pomoshch-yurista">Оспаривание
                                                        отцовства</a>
                                                </li>
                                                <li>
                                                    <a href="/pomoshch-po-alimentam-konsultaciya-yurist-spb">Помощь
                                                        по алиментам</a>
                                                </li>
                                                <li>
                                                    <span class="font-weight-bold">Бесплатные услуги</span>

                                                    <ul class="mt-3 px-0">
                                                        <li>
                                                            <a href="/konsultacija-yurista-po-alimentam-besplatno-onlain-spb">Консультация
                                                                юриста по алиментам</a>
                                                        </li>
                                                        <li>
                                                            <a href="/semejnye-konsultacii-yurista-spb">Семейные
                                                                консультации юриста</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="menu-specialization-item-15">
                                            <ul class="drop-list drop-list_two">
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="menu-specialization-item-16">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/vrachebnaya-oshibka-pomoshch-yurist-advokat">Врачебная
                                                        ошибка</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="menu-specialization-item-17">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/licenzirovanie-medicinskoj-deyatelnosti-spb">Лицензирование
                                                        медицинской деятельности</a>
                                                </li>
                                                <li>
                                                    <a href="/licenzirovanie-raboty-s-othodami-2017-spb">Лицензирование
                                                        работы с опасными отходами</a>
                                                </li>
                                                <li>
                                                    <a href="/licenziya-na-alkogol-pomoshch-v-poluchenii">Лицензия
                                                        на алкоголь</a>
                                                </li>
                                                <li>
                                                    <a href="/oformlenie-licenzii-mchs-yurist-spb">Получение
                                                        лицензии МЧС</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="menu-specialization-item-18">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/uklonenie-ot-uplaty-nalogov-sborov-organizacii-yurist">Уклонение
                                                        от уплаты налогов</a>
                                                </li>
                                                <li>
                                                    <a href="/nezakonnoe-predprinimatelstvo-deyatelnost-biznes-shtraf-nakazanie">Незаконное
                                                        предпренимательство</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-po-delam-o-hishchenii-statya-158-159-pomoshch-sud-spb-moskva">Хищение
                                                        чужого имущества</a>
                                                </li>
                                                <li>
                                                    <a href="/umyshlennoe-prichinenie-vreda-zdorovyu-statya-111-112-113-114-115">Причинение
                                                        вреда здоровью</a>
                                                </li>
                                                <li>
                                                    <a href="/kleveta-oskorblenie-statya-128-1-ugolovnyj-advokat-spb-moskva">Клевета</a>
                                                </li>
                                                <li>
                                                    <a href="/nezakonnoe-zavladenie-chuzhim-imushchestvom-ugolovnyj-yurist">Завладение
                                                        чужим имуществом</a>
                                                </li>
                                                <li>
                                                    <span class="font-weight-bold">Бесплатные услуги</span>

                                                    <ul class="mt-3 px-0">
                                                        <li>
                                                            <a href="/ugolovnyj-advokat-uslugi-pomoshch">Консультация
                                                                уголовного юриста</a>
                                                        </li>
                                                        <li>
                                                            <a href="/besplatnaya-yuridicheskaya-konsultaciya-advokata-po-ugolovnym-delam">Консультация
                                                                по уголовным вопросам</a>
                                                        </li>
                                                        <li>
                                                            <a href="/yuridicheskie-uslugi-po-ugolovnomu-pravu-advokat-yurist">Юридическая
                                                                помощь по уголовному праву</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="keywords-search d-block d-md-none">
                                            <span class="keywords-search__label">Поиск по ключевым словам:</span>
                                            <div class="keywords-search__row">
                                                <form action="https://yandex.ru/search/site/" method="get"
                                                      target="_blank" accept-charset="utf-8">
                                                    <input type="hidden" name="searchid" value="2305210">
                                                    <input type="hidden" name="l10n" value="ru">
                                                    <input type="hidden" name="reqenc" value="">
                                                    <input type="submit" class="submit" value="Найти">
                                                    <div class="keywords-search__text-wrap">
                                                        <input type="search" name="text"
                                                               class="text js-search-everything-input"
                                                               placeholder="Поиск по любому слову...">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="second-level-specialists pretty-links-decorator">
                                        <div class="drop-header">
                                            <div class="drop-header__title">
                                                        <span class="drop-header__title-icon">
                                                            <img class="lazy loaded" data-src="images/icon-spec.png" alt="" src="images/icon-spec.png" data-was-processed="true">
                                                        </span>
                                                <span class="drop-header__title-text">Специалисты</span>
                                            </div>
                                        </div>
                                        <div>
                                            <ul class="drop-list js-specialization-menu__specialist-chunk"
                                                style="display: none" data-id="menu-specialization-item-0">
                                                <li>
                                                    <a href="/zemelnyj-yurist-voprosy-spory-uslugi">Земельный
                                                        юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/zemelnyj-advokat-mezhevye-spory-snt-dnp">юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-po-snt-sadovodstvam-konsultaciya-uslugi">Юрист
                                                        по СНТ</a>
                                                </li>
                                                <li>
                                                    <a href="/publichnye-torgi-po-zemle-pomoshch-yurista">Публичные
                                                        торги по земле</a>
                                                </li>
                                                <li>
                                                    <a href="/publichnye-torgi-po-zemle-pomoshch-yurista">Публичные
                                                        торги по земле</a>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-0_5']"
                                                       data-collapsed-label="Все по аренде земли&nbsp;▿">
                                                        Все по аренде земли&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-0_5">
                                                        <li>
                                                            <a href="/arenda-zemli-zemelnogo-uchastka-avtostoyanki-parkovki">Аренда
                                                                земли под парковку</a>
                                                        </li>
                                                        <li>
                                                            <a href="/spor-po-arende-zemli-dogovoru-arendy">Спор
                                                                по аренде земли</a>
                                                        </li>
                                                        <li>
                                                            <a href="/sud-po-arende-zemli-spor-po-zemelnomu-pravu">Суд
                                                                по аренде земли</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-0_6']"
                                                       data-collapsed-label="
                                        Все по аренде земли&nbsp;▿
                                    ">
                                                        Все по аренде земли&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-0_6">
                                                        <li>
                                                            <a href="/arenda-zemli-zemelnogo-uchastka-avtostoyanki-parkovki">Аренда
                                                                земли под парковку</a>
                                                        </li>
                                                        <li>
                                                            <a href="/spor-po-arende-zemli-dogovoru-arendy">Спор
                                                                по аренде земли</a>
                                                        </li>
                                                        <li>
                                                            <a href="/sud-po-arende-zemli-spor-po-zemelnomu-pravu">Суд
                                                                по аренде земли</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-0_7']"
                                                       data-collapsed-label="
                                        Все по оформлению земли&nbsp;▿
                                    ">
                                                        Все по оформлению земли&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-0_7">
                                                        <li>
                                                            <a href="/zemelnoe-pravo-oformlenie-sobstvennost-spory-yurist-advokat">Оформление
                                                                земли в собственность</a>
                                                        </li>
                                                        <li>
                                                            <a href="/uslugi-po-zemleustrojstvu-pomoshch-yurista">Услуги
                                                                по землеустройству</a>
                                                        </li>
                                                        <li>
                                                            <a href="/zemelnyj-paj-vydelit-oformit-otmezhevat">Оформление
                                                                земельного пая</a>
                                                        </li>
                                                        <li>
                                                            <a href="/oformlenie-dachnoj-amnistii-na-zemlyu-uchastok-dom#">Оформление
                                                                дачной амнистии</a>
                                                        </li>
                                                        <li>
                                                            <a href="/ustanovlenie-i-izmenenie-granic-zemelnyh-uchastkov">Установление
                                                                и изменение границ земельных участков</a>
                                                        </li>
                                                        <li>
                                                            <a href="/oformlenie-granic-zemelnogo-uchastka-yurist-sud-spb">Оформление
                                                                границ земельного участка</a>
                                                        </li>
                                                        <li>
                                                            <a href="/dachnaya-amnistiya-poluchenie-razresheniya-na-stroitelstvo#">Дачная
                                                                амнистия на строительство</a>
                                                        </li>
                                                        <li>
                                                            <a href="/dachnaya-amnistiya-konsultacii-besplatno-spb">Дачная
                                                                амнистия на дом</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vykup-oformlenie-vydel-zemelnogo-paya-pomoshch-spb#">Выкуп,
                                                                оформление, выдел земельного пая</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vneocherednoe-priobretenie-zemelnyh-uchastkov">Внеочередное
                                                                приобретение земельных участков</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-0_8']"
                                                       data-collapsed-label="
                                        Все по оформлению земли&nbsp;▿
                                    ">
                                                        Все по оформлению земли&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-0_8">
                                                        <li>
                                                            <a href="/zemelnoe-pravo-oformlenie-sobstvennost-spory-yurist-advokat">Оформление
                                                                земли в собственность</a>
                                                        </li>
                                                        <li>
                                                            <a href="/uslugi-po-zemleustrojstvu-pomoshch-yurista">Услуги
                                                                по землеустройству</a>
                                                        </li>
                                                        <li>
                                                            <a href="/zemelnyj-paj-vydelit-oformit-otmezhevat">Оформление
                                                                земельного пая</a>
                                                        </li>
                                                        <li>
                                                            <a href="/oformlenie-dachnoj-amnistii-na-zemlyu-uchastok-dom#">Оформление
                                                                дачной амнистии</a>
                                                        </li>
                                                        <li>
                                                            <a href="/ustanovlenie-i-izmenenie-granic-zemelnyh-uchastkov">Установление
                                                                и изменение границ земельных участков</a>
                                                        </li>
                                                        <li>
                                                            <a href="/oformlenie-granic-zemelnogo-uchastka-yurist-sud-spb">Оформление
                                                                границ земельного участка</a>
                                                        </li>
                                                        <li>
                                                            <a href="/dachnaya-amnistiya-poluchenie-razresheniya-na-stroitelstvo#">Дачная
                                                                амнистия на строительство</a>
                                                        </li>
                                                        <li>
                                                            <a href="/dachnaya-amnistiya-konsultacii-besplatno-spb">Дачная
                                                                амнистия на дом</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vykup-oformlenie-vydel-zemelnogo-paya-pomoshch-spb#">Выкуп,
                                                                оформление, выдел земельного пая</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vneocherednoe-priobretenie-zemelnyh-uchastkov">Внеочередное
                                                                приобретение земельных участков</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-0_9']"
                                                       data-collapsed-label="
                                        Все по переводу земли&nbsp;▿
                                    ">
                                                        Все по переводу земли&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-0_9">
                                                        <li>
                                                            <a href="/izmenenie-vida-razreshennogo-ispolzovaniya-vri">Изменение
                                                                ВРИ</a>
                                                        </li>
                                                        <li>
                                                            <a href="/perevod-zemli-uchastka-pomoshch-yurist">Перевод
                                                                земли в иную категорию</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-0_10']"
                                                       data-collapsed-label="
                                        Все по переводу земли&nbsp;▿
                                    ">
                                                        Все по переводу земли&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-0_10">
                                                        <li>
                                                            <a href="/izmenenie-vida-razreshennogo-ispolzovaniya-vri">Изменение
                                                                ВРИ</a>
                                                        </li>
                                                        <li>
                                                            <a href="/perevod-zemli-uchastka-pomoshch-yurist">Перевод
                                                                земли в иную категорию</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-0_11']"
                                                       data-collapsed-label="
                                        Все для СНТ&nbsp;▿
                                    ">
                                                        Все для СНТ&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-0_11">
                                                        <li>
                                                            <a href="/yuridicheskoe-soprovozhdenie-deyatelnosti-snt">Юридическое
                                                                сопровождение СНТ</a>
                                                        </li>
                                                        <li>
                                                            <a href="/uslugi-snt-onsultaciya-yurist-besplatno">Услуги
                                                                для СНТ</a>
                                                        </li>
                                                        <li>
                                                            <a href="/sud-chlenskim-vznosam-snt-vzyskanie-uplata">Суд
                                                                по членским взносам</a>
                                                        </li>
                                                        <li>
                                                            <a href="/spory-chlenstvo-snt-vstuplenie-isklyuchenie-sud">Споры
                                                                по членству в СНТ</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-0_12']"
                                                       data-collapsed-label="
                                        Все для СНТ&nbsp;▿
                                    ">
                                                        Все для СНТ&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-0_12">
                                                        <li>
                                                            <a href="/yuridicheskoe-soprovozhdenie-deyatelnosti-snt">Юридическое
                                                                сопровождение СНТ</a>
                                                        </li>
                                                        <li>
                                                            <a href="/uslugi-snt-onsultaciya-yurist-besplatno">Услуги
                                                                для СНТ</a>
                                                        </li>
                                                        <li>
                                                            <a href="/sud-chlenskim-vznosam-snt-vzyskanie-uplata">Суд
                                                                по членским взносам</a>
                                                        </li>
                                                        <li>
                                                            <a href="/spory-chlenstvo-snt-vstuplenie-isklyuchenie-sud">Споры
                                                                по членству в СНТ</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-0_13']"
                                                       data-collapsed-label="
                                        Земельные споры&nbsp;▿
                                    ">
                                                        Земельные споры&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-0_13">
                                                        <li>
                                                            <a href="/zemelnye-spory-s-sosedyami">Земельные
                                                                споры с соседями</a>
                                                        </li>
                                                        <li>
                                                            <a href="/yuridicheskie-uslugi-zemelnym-voprosam-sporam">Юридические
                                                                услуги по земельным спорам</a>
                                                        </li>
                                                        <li>
                                                            <a href="/yuridicheskie-uslugi-zemelnym-voprosam-sporam">Юридическая
                                                                помощь по земельным спорам</a>
                                                        </li>
                                                        <li>
                                                            <a href="/spory-so-sluzhboj-po-zemelnomu-nadzoru-pomoshch-yurista">Споры
                                                                со службой по земельному надзору</a>
                                                        </li>
                                                        <li>
                                                            <a href="/izyatie-zemelnyh-uchastkov-zashchita-pomoshch-yurista">Защита
                                                                от изъятия земельных участков</a>
                                                        </li>
                                                        <li>
                                                            <a href="/otkaz-v-zemle-pod-izhs-pomoshch-yuista">Отказ
                                                                предоставить землю под ИЖС</a>
                                                        </li>
                                                        <li>
                                                            <a href="/osparivanie-zemelnogo-naloga">Оспаривание
                                                                земельного налога</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                            <ul class="drop-list js-specialization-menu__specialist-chunk"
                                                style="display: none" data-id="menu-specialization-item-1">
                                                <li>
                                                    <a href="/uslugi-geodezista-v-spb">Геодезисты в СПб и ЛО</a>
                                                </li>
                                                <li>
                                                    <a href="/uslugi-kadastrovogo-inzhenera-pomoshch-stoimost-konsultaciya">Услуги
                                                        кадастрового инженера</a>
                                                </li>
                                                <li>
                                                    <a href="/yuristy-kadastrovye-inzhenery-mezhevanie">Предложение
                                                        для кадастровых инженеров </a>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-1_3']"
                                                       data-collapsed-label="
                                        Все по межеванию&nbsp;▿
                                    ">
                                                        Все по межеванию&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-1_3">
                                                        <li>
                                                            <a href="/vynos-tochek-v-naturu-pomoshch-yurista">Вынос
                                                                точек в натуру</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vosstanovlenie-mezhevyh-znakov-dokumentov-zemlyu#">Восстановление
                                                                межевых знаков и документов на землю</a>
                                                        </li>
                                                        <li>
                                                            <a href="/prirezka-zemli-zemelnogo-uchastka-oformlenie">Прирезка
                                                                земли</a>
                                                        </li>
                                                        <li>
                                                            <a href="/mezhevye-spory-o-granicah-mezhdu-sosedyami">Решение
                                                                межевых споров</a>
                                                        </li>
                                                        <li>
                                                            <a href="/zemelnyj-dachnyj-spor-zabor-sosedi-sud">Споры
                                                                между соседями по установке забора</a>
                                                        </li>
                                                        <li>
                                                            <a href="/mezhevanie-zemelnogo-uchastka-leningradskoj-oblasti-zemelnyj-advokat">Стоимость
                                                                межевания ЗУ в ЛО</a>
                                                        </li>
                                                        <li>
                                                            <a href="/ustanovlenie-granic-zemelnogo-uchastka-mestnost-sud">Установление
                                                                границ земельного участка</a>
                                                        </li>
                                                        <li>
                                                            <a href="/utochnenie-granic-zemelnogo-uchastka-soglasovanie-mezhevanie">Уточнение
                                                                границ земельного участка</a>
                                                        </li>
                                                        <li>
                                                            <a href="/mezhevanie-zemli-uchastka-kadastrovye-raboty">Межевание</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-1_4']"
                                                       data-collapsed-label="
                                        Кадастровые работы&nbsp;▿
                                    ">
                                                        Кадастровые работы&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-1_4">
                                                        <li>
                                                            <a href="/kadastrovye-raboty-zemelnom-uchastke-provedenie">Выполнение
                                                                кадастровых работ</a>
                                                        </li>
                                                        <li>
                                                            <a href="/ispravlenie-tekhnicheskoj-kadastrovoj-oshibki-reestra-pomoshch-yurista">Исправление
                                                                реестровой ошибки</a>
                                                        </li>
                                                        <li>
                                                            <a href="/postanovka-kadastrovyj-uchet-uchastkov-nedvizhimosti">Постановка
                                                                на кадастровый учёт</a>
                                                        </li>
                                                        <li>
                                                            <a href="/ispravlenie-kadastrovogo-pasporta-pomoshch-yurista">Исправление
                                                                кадастрового паспорта</a>
                                                        </li>
                                                        <li>
                                                            <a href="/publichnaya-kadastrovaya-karta-pomoshch-konsultaciya">Публичная
                                                                кадастровая карта</a>
                                                        </li>
                                                        <li>
                                                            <a href="/samozahvat-zemli-uzakonit-oformit">Самозахват
                                                                земли</a>
                                                        </li>
                                                        <li>
                                                            <a href="/snizhenie-osparivanie-kadastrovoj-stoimosti#">Уменьшение
                                                                кадастровой стоимости</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-1_5']"
                                                       data-collapsed-label="
                                        Кадастровые работы&nbsp;▿
                                    ">
                                                        Кадастровые работы&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-1_5">
                                                        <li>
                                                            <a href="/kadastrovye-raboty-zemelnom-uchastke-provedenie">Выполнение
                                                                кадастровых работ</a>
                                                        </li>
                                                        <li>
                                                            <a href="/ispravlenie-tekhnicheskoj-kadastrovoj-oshibki-reestra-pomoshch-yurista">Исправление
                                                                реестровой ошибки</a>
                                                        </li>
                                                        <li>
                                                            <a href="/postanovka-kadastrovyj-uchet-uchastkov-nedvizhimosti">Постановка
                                                                на кадастровый учёт</a>
                                                        </li>
                                                        <li>
                                                            <a href="/ispravlenie-kadastrovogo-pasporta-pomoshch-yurista">Исправление
                                                                кадастрового паспорта</a>
                                                        </li>
                                                        <li>
                                                            <a href="/publichnaya-kadastrovaya-karta-pomoshch-konsultaciya">Публичная
                                                                кадастровая карта</a>
                                                        </li>
                                                        <li>
                                                            <a href="/samozahvat-zemli-uzakonit-oformit">Самозахват
                                                                земли</a>
                                                        </li>
                                                        <li>
                                                            <a href="/snizhenie-osparivanie-kadastrovoj-stoimosti#">Уменьшение
                                                                кадастровой стоимости</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-1_6']"
                                                       data-collapsed-label="
                                        Геодезические работы&nbsp;▿
                                    ">
                                                        Геодезические работы&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-1_6">
                                                        <li>
                                                            <a href="/geodezicheskaya-semka-zemelnogo-uchastka-mestnosti-zdanij">Геодезическая
                                                                съемка земельного участка</a>
                                                        </li>
                                                        <li>
                                                            <a href="/kontrolno-ispolnitelnaya-semka-kommunikacij-setej-obekta">Исполнительная
                                                                съёмка</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-1_7']"
                                                       data-collapsed-label="
                                        Геодезические работы&nbsp;▿
                                    ">
                                                        Геодезические работы&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-1_7">
                                                        <li>
                                                            <a href="/geodezicheskaya-semka-zemelnogo-uchastka-mestnosti-zdanij">Геодезическая
                                                                съемка земельного участка</a>
                                                        </li>
                                                        <li>
                                                            <a href="/kontrolno-ispolnitelnaya-semka-kommunikacij-setej-obekta">Исполнительная
                                                                съёмка</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-1_8']"
                                                       data-collapsed-label="
                                        Операции с землей&nbsp;▿
                                    ">
                                                        Операции с землей&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-1_8">
                                                        <li>
                                                            <a href="/vydel-zemli-nature-doli-arenda">Выдел
                                                                земли</a>
                                                        </li>
                                                        <li>
                                                            <a href="/skhema-raspolozheniya-zemelnogo-uchastka-pomoshch-yurista">Схема
                                                                расположения земельного участка</a>
                                                        </li>
                                                        <li>
                                                            <a href="/obedinenie-zemelnyh-uchastkov-pereraspredelenie">Объединение
                                                                земельных участков</a>
                                                        </li>
                                                        <li>
                                                            <a href="/razdel-zemli-doma-uchastka-doli-granicy-sud">Раздел
                                                                земли</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                            <ul class="drop-list js-specialization-menu__specialist-chunk"
                                                style="display: none" data-id="menu-specialization-item-2">
                                                <li>
                                                    <a href="/antikollektory-uslugi-pomoshch-besplatno">Антиколлекторы
                                                        N1 </a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-bankovskim-delam-voprosam-konsultaciya">Банковский
                                                        юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/kreditnyj-yurist-spory-konsultaciya">Кредитный
                                                        юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-po-kreditam-dolgam-yurist-po-ipoteke">юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-dolg-spb">Юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-dolg-spb">юрист по долгам</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-bankrotstvu-fizicheskih-lic-konsultaciya">Юрист
                                                        по банкротству физ.лиц</a>
                                                </li>
                                                <li>
                                                    <a href="/ipotechnyj-advokat-pomoshch-besplatno-spb">Юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/ipotechnyj-yurist-pomoshch">юрист по ипотеке</a>
                                                </li>
                                                <li>
                                                    <a href="/kreditnyj-finansovyj-ipotechnyj-konsultant">Финансовый,
                                                        кредитный, ипотечный консультант</a>
                                                </li>
                                            </ul>
                                            <ul class="drop-list js-specialization-menu__specialist-chunk"
                                                style="display: none" data-id="menu-specialization-item-3">
                                                <li>
                                                    <a href="/advokat-nedvizhimosti-konsultaciya-besplatno">Юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-po-nedvizhimosti-konsultaciya">юрист по
                                                        недвижимости</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-perevod-nedvizhimosti-spb-moskva">Юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-perevod-nedvizhimosti-spb-moskva">юрист по
                                                        переводу недвижимости</a>
                                                </li>
                                                <li>
                                                    <a href="/kvartirnyj-advokat-zhilishchnyj-vopros-konsultacii-onlain-besplatno#">Квартирный
                                                        юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-kvartirnym-voprosam-pokupka-prodazha-oformit">Юрист
                                                        по квартирным вопросам</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-po-privatizacii-kvartiry-komnaty-zemli-doma">Юрист
                                                        по приватизации</a>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-3_7']"
                                                       data-collapsed-label="
                                        Перевод недвижимости&nbsp;▿
                                    ">
                                                        Перевод недвижимости&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-3_7">
                                                        <li>
                                                            <a href="/perevod-iz-dolevoj-sobstvennosti-nedvizhimosti-v-sovmestnuyu">Перевод
                                                                из долевой собственности</a>
                                                        </li>
                                                        <li>
                                                            <a href="/perevod-pomeshcheniya-yurist-advokat-spb-moskva">Перевод
                                                                недвижимости в ипотеке</a>
                                                        </li>
                                                        <li>
                                                            <a href="/perevod-nedvizhimosti-spb-moskva">Перевод
                                                                недвижимости цена, стоимость</a>
                                                        </li>
                                                        <li>
                                                            <a href="/priznanie-zhilya-pomeshcheniya-doma-nezhilym-konsultacii-yurist">Признание
                                                                помещения нежилым</a>
                                                        </li>
                                                        <li>
                                                            <a href="/konsultaciya-yurista-advokata-po-perevodu-v-nezhiloj-fond-spb-moskva">Консультация
                                                                по переводу в нежилой фонд</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                            <ul class="drop-list js-specialization-menu__specialist-chunk"
                                                style="display: none" data-id="menu-specialization-item-4">
                                                <li>
                                                    <a href="/zhilishchnyj-advokat-spory-konsultaciya-besplatno">Жилищный
                                                        юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-po-zhilishchnym-voprosam-v-vashem-rajone-advokat">юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/voennyj-zhilishchnyj-yurist-konsultacii-besplatno-ipoteka-yurist-advokat">Военный
                                                        жилищный юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-po-zhilishchnym-voprosam-v-vashem-rajone-advokat">Жилищный
                                                        юрист в вашем районе</a>
                                                </li>
                                            </ul>
                                            <ul class="drop-list js-specialization-menu__specialist-chunk"
                                                style="display: none" data-id="menu-specialization-item-5">
                                                <li>
                                                    <a href="/yurist-po-ddu-pomoshch-vzyskanie-neustojki">Юрист
                                                        по ДДУ</a>
                                                </li>
                                                <li>
                                                    <a href="/pomoshch-pajshchikam-zhk-sily-prirody">Помощь
                                                        пайщикам ЖК "Силы природы"</a>
                                                </li>
                                                <li>
                                                    <a href="/bankrotstvo-zastrojshchika-o2-development-zhk-sily-prirody">Банкротство
                                                        О2 Development </a>
                                                </li>
                                                <li>
                                                    <a href="/bankrotstvo-zastrojshchika-ooo-romantika">Банкротство
                                                        «Романтика» </a>
                                                </li>
                                                <li>
                                                    <a href="/vzyskanie-neustojki-sud-ooo-polis-grupp">Полис
                                                        Групп</a>
                                                </li>
                                                <li>
                                                    <a href="/spor-s-zastrojshchikom-normann-dolevoe-uchastie">Норманн</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-s-ooo-lenspecstroj-vzyskanie-neustojki-sud">ЛенСпецСтрой</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-s-zastrojshchikom-ooo-stroitelnaya-kompaniya-dalpiterstroj">Дальпитерстрой</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-s-zastrojshchikom-ooo-spb-renovaciya">СПб
                                                        Реновация</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-s-zastrojshchikom-ooo-glavstroj-spb">Главстрой
                                                        - СПб</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-gk-lider-grupp">Лидер Групп</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-s-zastrojshchikom-po-ddu-gk-pik">ПИК</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-ao-sk-ips">ИПС</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-ooo-petrostroj">Петрострой</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-ooo-neva-stroj-invest">Нева
                                                        Строй Инвест</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-ooo-l1-4-lehk">Л1-4
                                                        (ЛЭК)</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-petrotrest">Петротрест</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-centr-dolevogo-stroitelstva-cds">Центр
                                                        Долевого Строительства (ЦДС)</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-legenda">ГК
                                                        "Легенда"</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-lenspecsmu">ЛенСпецСМУ</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-su-155">СУ-155</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-rosstroj">Россторой</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-titan">ООО
                                                        "Титан"</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-gk-gorod">ГК
                                                        "Город"</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-lsr">ЛСР</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-sehtl-siti">Сэтл
                                                        Сити (Setl City)</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-severnyj-gorod">Северный
                                                        город</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-arsenal-nedvizhimost">Арсенал-недвижимость</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-rbi">RBI</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-rosstrojinvest">РосСтройИнвест</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-lider">Лидер</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-gk-ehtalon">ГК
                                                        "Эталон"</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-stroitelnoe-upravlenie">Строительное
                                                        управление</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-impuls">Импульс</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-vozrozhdenie-sankt-peterburga">Возрождение
                                                        Санкт-Петербурга</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-petropol">Петрополь</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-47-trest">47 трест</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-s-zastrojshchikom-aag">AAG</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-s-zastrojshchikom-aiber-group">Aiber
                                                        Group</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-s-zastrojshchikom-bonava-ncc">Bonava
                                                        (NCC)</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-s-zastrojshchikom-po-ddu-s-astra">ГК
                                                        "Астра"</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-po-ddu-s-zastrojshchikom-glorax-development">Glorax
                                                        Development</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-s-zastrojshchikom-yuit-sankt-peterburg">ЮИТ</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-s-zastrojshchikom-po-ddu-s-eke">EKE</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-s-zastrojshchikom-imd-group">IMD Group</a>
                                                </li>
                                                <li>
                                                    <a href="/spory-s-zastrojshchikom-severnyj-gorod">Северный
                                                        Город</a>
                                                </li>
                                            </ul>
                                            <ul class="drop-list js-specialization-menu__specialist-chunk"
                                                style="display: none" data-id="menu-specialization-item-6">
                                                <li>
                                                    <a href="/advokat-po-zaveshchaniyam">Юрист по завещаниям</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-po-zaveshchaniyam-spb">Юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-nasledstvo-prinyatie-oformlenie-vosstanovlenie">Наследственный
                                                        юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/nasledstvennyj-yurist-zaveshchaniya-priznanie-prava-nedostojnym-sud">юрист</a>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-6_4']"
                                                       data-collapsed-label="
                                        Оформление наследства&nbsp;▿
                                    ">
                                                        Оформление наследства&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-6_4">
                                                        <li>
                                                            <a href="/oformlenie-nasledstva-za-granicej-yuristy-advokaty-spb-moskva">Оформление
                                                                наследства за границей</a>
                                                        </li>
                                                        <li>
                                                            <a href="/oformit-nasledstvo-v-sobstvennost-konsultacii-besplatno">Оформление
                                                                наследства в собственность</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vstuplenie-v-nasledstvo-po-zaveshchaniyu-konsultacii-besplatno#">Вступление
                                                                в наследство</a>
                                                        </li>
                                                        <li>
                                                            <a href="/otkrytie-nasledstva-spb-moskva-pomoshch-yurista">Открытие
                                                                наследства</a>
                                                        </li>
                                                        <li>
                                                            <a href="/nasledovanie-biznesa-yuridicheskoe-soprovozhdenie-spb-moskva">Наследование
                                                                бизнеса</a>
                                                        </li>
                                                        <li>
                                                            <a href="/nasledstvo-imushchestva-konsultacii-besplatno-spb-moskva">Наследование
                                                                имущества</a>
                                                        </li>
                                                        <li>
                                                            <a href="/nasledstvo-kvartiry-oformlenie-vstuplenie-spb-moskva">Наследование
                                                                квартиры</a>
                                                        </li>
                                                        <li>
                                                            <a href="/stoimost-uslug-po-oformleniyu-nasledstva-spb-moskva">Услуги
                                                                по оформлению наследства</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-6_5']"
                                                       data-collapsed-label="
                                        Все по принятию наследства&nbsp;▿
                                    ">
                                                        Все по принятию наследства&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-6_5">
                                                        <li>
                                                            <a href="/priznanie-prava-sobstvennosti-na-nasledstvo-spb-moskva">Признание
                                                                права собственности на наследство</a>
                                                        </li>
                                                        <li>
                                                            <a href="/fakticheskoe-prinyatie-nasledstva-dokazat-osporit-yurist-sud">Фактическое
                                                                принятие наследства</a>
                                                        </li>
                                                        <li>
                                                            <a href="/nasledstvo-po-zaveshchaniyu-konsultacii-besplatno-spb-moskva">Принятие
                                                                наследства по завещанию</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-6_6']"
                                                       data-collapsed-label="
                                        Все по принятию наследства&nbsp;▿
                                    ">
                                                        Все по принятию наследства&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-6_6">
                                                        <li>
                                                            <a href="/priznanie-prava-sobstvennosti-na-nasledstvo-spb-moskva">Признание
                                                                права собственности на наследство</a>
                                                        </li>
                                                        <li>
                                                            <a href="/fakticheskoe-prinyatie-nasledstva-dokazat-osporit-yurist-sud">Фактическое
                                                                принятие наследства</a>
                                                        </li>
                                                        <li>
                                                            <a href="/nasledstvo-po-zaveshchaniyu-konsultacii-besplatno-spb-moskva">Принятие
                                                                наследства по завещанию</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-6_7']"
                                                       data-collapsed-label="
                                        Споры по наследству&nbsp;▿
                                    ">
                                                        Споры по наследству&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-6_7">
                                                        <li>
                                                            <a href="/osparivanie-zaveshchaniya-pomoshch-yurista-spb">Оспаривание
                                                                завещания</a>
                                                        </li>
                                                        <li>
                                                            <a href="/dolgi-nasledstvu-osparivanie-konsultaciya-besplatno-yurist">Оспаривание
                                                                долгов по наследству</a>
                                                        </li>
                                                        <li>
                                                            <a href="/razdel-nasledstva-imushchestva-doli-doma-sud">Раздел
                                                                наследства</a>
                                                        </li>
                                                        <li>
                                                            <a href="/osparivanie-zaveshchaniya-yuristy-advokaty-spb-moskva">Получение
                                                                наследства через суд</a>
                                                        </li>
                                                        <li>
                                                            <a href="/otkaz-ot-nasledstva-sostavlenie-zayavleniya-spb-moskva">Отказ
                                                                от наследства</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                            <ul class="drop-list js-specialization-menu__specialist-chunk"
                                                style="display: none" data-id="menu-specialization-item-7">
                                                <li>
                                                    <a href="/trudovoj-advokat-spory-prava-voprosy">Трудовой
                                                        юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/trudovoj-yurist-spory-konsultacii-besplatno">юрист</a>
                                                </li>
                                            </ul>
                                            <ul class="drop-list js-specialization-menu__specialist-chunk"
                                                style="display: none" data-id="menu-specialization-item-8">
                                                <li>
                                                    <a href="/stroitelnyj-yurist-konsultacii">Строительный
                                                        юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-stroitelnye-dela-spory-voprosy-uslugi-sud">юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-dolevomu-stroitelstvu-dolevka-ddu-214-fz">Юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-dolevomu-stroitelstvu-ddu-214-fz">юрист по
                                                        долевому строительству</a>
                                                </li>
                                            </ul>
                                            <ul class="drop-list js-specialization-menu__specialist-chunk"
                                                style="display: none" data-id="menu-specialization-item-9">
                                                <li>
                                                    <a href="/arbitrazhnyj-advokat-arbitrazhnye-dela">Арбитражный
                                                        юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/arbitrazhnyj-yurist-arbitrazhnye-dela-spory-voprosy-konsultaciya-sud">юрист</a>
                                                </li>
                                            </ul>
                                            <ul class="drop-list js-specialization-menu__specialist-chunk"
                                                style="display: none" data-id="menu-specialization-item-10">
                                                <li>
                                                    <a href="/advokat-vzyskaniyu-dolgov-alimentov-neustojki-zadolzhennosti">Юрист
                                                        по взысканию</a>
                                                </li>
                                                <li>
                                                    <a href="/vzyskanie-neustojki-arbitrazhnyj-sud-praktika">Взыскание
                                                        неустойки в Арбитражном суде</a>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-10_2']"
                                                       data-collapsed-label="
                                        Взыскание неустоек&nbsp;▿
                                    ">
                                                        Взыскание неустоек&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-10_2">
                                                        <li>
                                                            <a href="/vzyskanie-neustojki-arbitrazhnyj-sud-praktika">Взыскание
                                                                неустойки в арбитражном суде</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vzyskanie-neustojki-strahovoj-kompanii-osago-kasko">Взыскание
                                                                неустойки со страховой компании</a>
                                                        </li>
                                                        <li>
                                                            <a href="/srok-vzyskaniya-neustojki-yurist-spb-moskva">Срок
                                                                взыскания неустойки</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-10_3']"
                                                       data-collapsed-label="
                                        Взыскание неустоек&nbsp;▿
                                    ">
                                                        Взыскание неустоек&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-10_3">
                                                        <li>
                                                            <a href="/vzyskanie-neustojki-arbitrazhnyj-sud-praktika">Взыскание
                                                                неустойки в арбитражном суде</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vzyskanie-neustojki-strahovoj-kompanii-osago-kasko">Взыскание
                                                                неустойки со страховой компании</a>
                                                        </li>
                                                        <li>
                                                            <a href="/srok-vzyskaniya-neustojki-yurist-spb-moskva">Срок
                                                                взыскания неустойки</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-10_4']"
                                                       data-collapsed-label="
                                        Взыскание убытков&nbsp;▿
                                    ">
                                                        Взыскание убытков&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-10_4">
                                                        <li>
                                                            <a href="/vzyskanie-ubytkov-arbitrazhnom-sude-obshchej-yurisdikcii">Взыскание
                                                                убытков в арбитражном суде</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vzyskanie-ubytkov-dogovoru-podryada-postavki-arendy">Взыскание
                                                                по договору подряда</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vzyskanie-ubytkov-bankrotstv%D0%BE-rukovoditelya-banka">Взыскание
                                                                убытков при банкротстве</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vzyskanie-ubytkov-generalnogo-byvshego-direktora">Взыскание
                                                                убытков с генерального директора</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vzyskanie-zastrojshchika-ubytkov-neustojki-spb">Взыскание
                                                                убытков с застройщика</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vzyskanie-ubytkov-s-zakazchika-po-dogovoru-podryada-konsultacii-yurist">Взыскание
                                                                с заказчика</a>
                                                        </li>
                                                        <li>
                                                            <a href="/yuridicheskie-uslugi-po-vzyskaniyu-pomoshch-sud-advokat">Юридические
                                                                услуги по взысканию</a>
                                                        </li>
                                                        <li>
                                                            <a href="/nekachestvennyj-remont">Взыскание за
                                                                некачественный ремонт</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-10_5']"
                                                       data-collapsed-label="
                                        Взыскание убытков&nbsp;▿
                                    ">
                                                        Взыскание убытков&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-10_5">
                                                        <li>
                                                            <a href="/vzyskanie-ubytkov-arbitrazhnom-sude-obshchej-yurisdikcii">Взыскание
                                                                убытков в арбитражном суде</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vzyskanie-ubytkov-dogovoru-podryada-postavki-arendy">Взыскание
                                                                по договору подряда</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vzyskanie-ubytkov-bankrotstv%D0%BE-rukovoditelya-banka">Взыскание
                                                                убытков при банкротстве</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vzyskanie-ubytkov-generalnogo-byvshego-direktora">Взыскание
                                                                убытков с генерального директора</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vzyskanie-zastrojshchika-ubytkov-neustojki-spb">Взыскание
                                                                убытков с застройщика</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vzyskanie-ubytkov-s-zakazchika-po-dogovoru-podryada-konsultacii-yurist">Взыскание
                                                                с заказчика</a>
                                                        </li>
                                                        <li>
                                                            <a href="/yuridicheskie-uslugi-po-vzyskaniyu-pomoshch-sud-advokat">Юридические
                                                                услуги по взысканию</a>
                                                        </li>
                                                        <li>
                                                            <a href="/nekachestvennyj-remont">Взыскание за
                                                                некачественный ремонт</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-10_6']"
                                                       data-collapsed-label="
                                        Взыскание ущерба&nbsp;▿
                                    ">
                                                        Взыскание ущерба&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-10_6">
                                                        <li>
                                                            <a href="/vzyskanie-materialnogo-ushcherba-zashchita-v-sude">Взыскание
                                                                материального ущерба</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vzyskanie-ushcherba-za-moralnyj-vred-polnaya-kompensaciya-konsultacii-besplatno-yurist">Взыскание
                                                                морального ущерба</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vzyskanie-ushcherba-ot-prestupleniya-kompensaciya-spb">Взыскание
                                                                ущерба от преступления</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vzyskanie-ushcherba-s-rabotnika-sotrudnika-konsultacii-onlain-besplatno-yurist">Взыскание
                                                                ущерба с работника</a>
                                                        </li>
                                                        <li>
                                                            <a href="/vzyskanie-ushcherba-sud-sankt-peterburg">Взыскание
                                                                ущерба через суд</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                            <ul class="drop-list js-specialization-menu__specialist-chunk"
                                                style="display: none" data-id="menu-specialization-item-11">
                                                <li>
                                                    <a href="/avtomobilnyj-advokat">Автомобильный юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/avtomobilnyj-yurist-konsultaciya-besplatno">юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-dtp-pomoshch-avariyah-yurist-konsultacii">Юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-dtp-besplatno-pomoshch-avariya-konsultacii">юрист
                                                        по ДТП</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-pdd-konsultaciya-spb">Юрист по ПДД </a>
                                                </li>
                                            </ul>
                                            <ul class="drop-list js-specialization-menu__specialist-chunk"
                                                style="display: none" data-id="menu-specialization-item-12">
                                                <li>
                                                    <a href="/pensionnyj-yurist-konsultaciya">Пенсионный
                                                        юрист</a>
                                                </li>
                                            </ul>
                                            <ul class="drop-list js-specialization-menu__specialist-chunk"
                                                style="display: none" data-id="menu-specialization-item-13">
                                                <li>
                                                    <a href="/yurist-avtorskomu-pravu-zashchita-pomoshch-konsultaciya">Юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/avtorskoe-pravo-intellektualnaya-sobstvennost">юрист
                                                        по авторскому праву</a>
                                                </li>
                                            </ul>
                                            <ul class="drop-list js-specialization-menu__specialist-chunk"
                                                style="display: none" data-id="menu-specialization-item-14">
                                                <li>
                                                    <a href="/semejnyj-yurist-besplatnaya-konsultaciya">Семейный
                                                        юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/semejnyj-advokat-pomoshch-spb-moskva">юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/brakorazvodnyj-yurist-besplatnaya-konsultaciya-spb">Бракоразводный
                                                        юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/brakorazvodnyj-advokat-besplatnaya-konsultaciya-spb">юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-po-razvodam-konsultaciya-besplatno-spb">Юрист
                                                        по разводу/</a>
                                                </li>
                                                <li>
                                                    <a href="/brakorazvodnyj-advokat-besplatnaya-konsultaciya-spb">юрист
                                                        по разводу</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-po-razdelu-imushchestva">Юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-po-razdelu-imushchestva">юрист по разделу
                                                        имущества</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-po-alimentam-pomoshch">Юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-po-alimentam-pomoshch">юрист по
                                                        алиментам</a>
                                                </li>
                                                <li>
                                                    <a href="/poryadok-obshcheniya-s-detmi-yurist-spb">Юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/poryadok-obshcheniya-s-detmi-advokat-spb">юрист по
                                                        определению порядка общения с детьми</a>
                                                </li>
                                                <li>
                                                    <a href="/lishenie-vosstanovlenie-roditelskih-prav-sud-yurist">Юрист
                                                        по лишению/восстановлению родительских прав</a>
                                                </li>
                                                <li>
                                                    <a href="/lishenie-vosstanovlenie-roditelskih-prav-sud-advokat">Юрист
                                                        по по лишению/восстановлению родительских прав</a>
                                                </li>
                                            </ul>
                                            <ul class="drop-list js-specialization-menu__specialist-chunk"
                                                style="display: none" data-id="menu-specialization-item-15">
                                                <li>
                                                    <a href="/yurist-pasport-grazhdanina-rossii">Паспорт
                                                        гражданина России</a>
                                                </li>
                                            </ul>
                                            <ul class="drop-list js-specialization-menu__specialist-chunk"
                                                style="display: none" data-id="menu-specialization-item-16">
                                                <li>
                                                    <a href="/medicinskij-yurist-konsultaciya-pomoshch-spb">Медицинский
                                                        юрист/</a>
                                                </li>
                                                <li>
                                                    <a href="/medicinskij-advokat-voprosy-spory-konsultaciya">юрист</a>
                                                </li>
                                            </ul>
                                            <ul class="drop-list js-specialization-menu__specialist-chunk"
                                                style="display: none" data-id="menu-specialization-item-17">
                                                <li>
                                                    <a href="/pomoshch-v-poluchenii-licenzii-na-osushchestvlenie">Помощь
                                                        в получении лицензии</a>
                                                </li>
                                            </ul>
                                            <ul class="drop-list js-specialization-menu__specialist-chunk"
                                                style="display: none" data-id="menu-specialization-item-18">
                                                <li>
                                                    <a href="/ugolovnyj-advokat-uslugi-pomoshch">Уголовный/</a>
                                                </li>
                                                <li>
                                                    <a href="/kriminalnyj-advokat-pomoshch-po-ugolovnomu-delu-yurist">криминальный
                                                        юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-po-ehkonomicheskim-prestupleniyam-pomoshch-zashchita-sud">Юрист
                                                        по экономическим преступлениям</a>
                                                </li>
                                                <li>
                                                    <a href="/ugolovnyj-advokat-po-narkotikam-statya-228-234">Оборот
                                                        наркотиков</a>
                                                </li>
                                                <li>
                                                    <a href="/ugolovnyj-advokat-po-narkotikam-statya-228-234">Оборот
                                                        наркотиков</a>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-18_5']"
                                                       data-collapsed-label="
                                        Экономические преступления&nbsp;▿
                                    ">
                                                        Экономические преступления&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-18_5">
                                                        <li>
                                                            <a href="/uklonenie-ot-uplaty-nalogov-sborov-fizicheskimi-licami">Уклонение
                                                                от уплаты налогов</a>
                                                        </li>
                                                        <li>
                                                            <a href="/poluchenie-dacha-vzyatki-oborotni-v-pogonah-konsultaciya-advokata#">Получениедача
                                                                взятки</a>
                                                        </li>
                                                        <li>
                                                            <a href="/nezakonnoe-poluchenie-kredita-statya-176-yurist-advokat-spb-moskva">Незаконное
                                                                получение кредита</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-po-otmyvaniyu-denezhnyh-sredstv-spb-moskva">Отмывание
                                                                денег</a>
                                                        </li>
                                                        <li>
                                                            <a href="/necelevoe-raskhodovanie-byudzhetnyh-sredstv-statya-285">Нецелевое
                                                                расходование бюджетных средств</a>
                                                        </li>
                                                        <li>
                                                            <a href="/nezakonnoe-zavladenie-chuzhim-imushchestvom-ugolovnyj-yurist">Незаконное
                                                                завладение чужим имуществом</a>
                                                        </li>
                                                        <li>
                                                            <a href="/zloupotreblenie-polnomochiyami-advokat-sud-spb-moskva">Злоупотребление
                                                                полномочиями</a>
                                                        </li>
                                                        <li>
                                                            <a href="/rastrata-chuzhogo-vverennogo-imushchestva-st-160-ugolovnyj-advokat#">Растрата
                                                                вверенного имущества</a>
                                                        </li>
                                                        <li>
                                                            <a href="/fiktivnoe-prednamerennoe-bankrotstvo-otvetstvennost-statya-197">Фиктивное
                                                                банкротство</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-18_6']"
                                                       data-collapsed-label="
                                        Экономические преступления&nbsp;▿
                                    ">
                                                        Экономические преступления&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-18_6">
                                                        <li>
                                                            <a href="/uklonenie-ot-uplaty-nalogov-sborov-fizicheskimi-licami">Уклонение
                                                                от уплаты налогов</a>
                                                        </li>
                                                        <li>
                                                            <a href="/poluchenie-dacha-vzyatki-oborotni-v-pogonah-konsultaciya-advokata#">Получениедача
                                                                взятки</a>
                                                        </li>
                                                        <li>
                                                            <a href="/nezakonnoe-poluchenie-kredita-statya-176-yurist-advokat-spb-moskva">Незаконное
                                                                получение кредита</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-po-otmyvaniyu-denezhnyh-sredstv-spb-moskva">Отмывание
                                                                денег</a>
                                                        </li>
                                                        <li>
                                                            <a href="/necelevoe-raskhodovanie-byudzhetnyh-sredstv-statya-285">Нецелевое
                                                                расходование бюджетных средств</a>
                                                        </li>
                                                        <li>
                                                            <a href="/nezakonnoe-zavladenie-chuzhim-imushchestvom-ugolovnyj-yurist">Незаконное
                                                                завладение чужим имуществом</a>
                                                        </li>
                                                        <li>
                                                            <a href="/zloupotreblenie-polnomochiyami-advokat-sud-spb-moskva">Злоупотребление
                                                                полномочиями</a>
                                                        </li>
                                                        <li>
                                                            <a href="/rastrata-chuzhogo-vverennogo-imushchestva-st-160-ugolovnyj-advokat#">Растрата
                                                                вверенного имущества</a>
                                                        </li>
                                                        <li>
                                                            <a href="/fiktivnoe-prednamerennoe-bankrotstvo-otvetstvennost-statya-197">Фиктивное
                                                                банкротство</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-18_7']"
                                                       data-collapsed-label="
                                        Незаконное предпринимательство &nbsp;▿
                                    ">
                                                        Незаконное предпринимательство &nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-18_7">
                                                        <li>
                                                            <a href="/advokat-po-delam-nezakonnogo-ehksporta-iz-rossii-statya-189">Незаконный
                                                                экспорт</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-realizaciya-tabachnyh-izdelij-spb-moskva">Незаконная
                                                                реализация табачных изделий</a>
                                                        </li>
                                                        <li>
                                                            <a href="/hranenie-sbyt-nezakonno-priobretennoj-drevesiny-yurist-advokat">Незаконный
                                                                оборот древесины</a>
                                                        </li>
                                                        <li>
                                                            <a href="/nezakonnnaya-bankovskaya-deyatelnost-statya-172-ugolovnyj-advokat">Незаконная
                                                                банковская деятельность</a>
                                                        </li>
                                                        <li>
                                                            <a href="/nezakonnye-organizaciya-provedenie-azartnyh-igr-yurist">Незаконное
                                                                проведение азартных игр</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-po-kontrabande-statya-188-229-1-226-1-zashchita-v-sude-yuristy">Контрабанда</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-18_8']"
                                                       data-collapsed-label="
                                        Незаконное предпринимательство &nbsp;▿
                                    ">
                                                        Незаконное предпринимательство &nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-18_8">
                                                        <li>
                                                            <a href="/advokat-po-delam-nezakonnogo-ehksporta-iz-rossii-statya-189">Незаконный
                                                                экспорт</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-realizaciya-tabachnyh-izdelij-spb-moskva">Незаконная
                                                                реализация табачных изделий</a>
                                                        </li>
                                                        <li>
                                                            <a href="/hranenie-sbyt-nezakonno-priobretennoj-drevesiny-yurist-advokat">Незаконный
                                                                оборот древесины</a>
                                                        </li>
                                                        <li>
                                                            <a href="/nezakonnnaya-bankovskaya-deyatelnost-statya-172-ugolovnyj-advokat">Незаконная
                                                                банковская деятельность</a>
                                                        </li>
                                                        <li>
                                                            <a href="/nezakonnye-organizaciya-provedenie-azartnyh-igr-yurist">Незаконное
                                                                проведение азартных игр</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-po-kontrabande-statya-188-229-1-226-1-zashchita-v-sude-yuristy">Контрабанда</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-18_9']"
                                                       data-collapsed-label="
                                        Чужое имущество&nbsp;▿
                                    ">
                                                        Чужое имущество&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-18_9">
                                                        <li>
                                                            <a href="/krazha-imushchestva-statya-158-ugolovnyj-advokat-spb-moskva#">Кража</a>
                                                        </li>
                                                        <li>
                                                            <a href="/grabezh-statya-161-yuristy-spb-moskva">Грабеж</a>
                                                        </li>
                                                        <li>
                                                            <a href="/razboj-statya-162-spb-moskva#">Разбой</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-po-delam-ob-unichtozhenii-chuzhogo-imushchestva-statya-167">Уничтожение
                                                                чужого имущества</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-18_10']"
                                                       data-collapsed-label="
                                        Чужое имущество&nbsp;▿
                                    ">
                                                        Чужое имущество&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-18_10">
                                                        <li>
                                                            <a href="/krazha-imushchestva-statya-158-ugolovnyj-advokat-spb-moskva#">Кража</a>
                                                        </li>
                                                        <li>
                                                            <a href="/grabezh-statya-161-yuristy-spb-moskva">Грабеж</a>
                                                        </li>
                                                        <li>
                                                            <a href="/razboj-statya-162-spb-moskva#">Разбой</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-po-delam-ob-unichtozhenii-chuzhogo-imushchestva-statya-167">Уничтожение
                                                                чужого имущества</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-18_11']"
                                                       data-collapsed-label="
                                        Тяжкие преступления&nbsp;▿
                                    ">
                                                        Тяжкие преступления&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-18_11">
                                                        <li>
                                                            <a href="/prinuzhdenie-k-dejstviyam-seksualnogo-haraktera-statya-133-ugolovnyj-advokat">Принуждение
                                                                к действиям сексуального характера</a>
                                                        </li>
                                                        <li>
                                                            <a href="/iznasilovanie-statya-131-ugolovnyj-advokat-spb-moskva">Изнасилование</a>
                                                        </li>
                                                        <li>
                                                            <a href="/nasilstvennye-dejstviya-seksualnogo-haraktera-statya-132-ugolovny-advokat">Ст.132</a>
                                                        </li>
                                                        <li>
                                                            <a href="/polovoe-snoshenie-licom-ne-dostigshim-shestnadcatiletnego-vozrasta-statya-134">Ст.134</a>
                                                        </li>
                                                        <li>
                                                            <a href="/poboi-istyazanie-statya-116-117-ugolovnyj-advokat-spb-moskva#">Побои,
                                                                истязания</a>
                                                        </li>
                                                        <li>
                                                            <a href="/razvratnye-dejstviya-statya-135-yuristy-advokaty">Развратные
                                                                действия</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-18_12']"
                                                       data-collapsed-label="
                                        &nbsp;▿
                                    ">
                                                        &nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-18_12">
                                                        <li>
                                                            <a href="/"></a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-18_13']"
                                                       data-collapsed-label="
                                        &nbsp;▿
                                    ">
                                                        &nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-18_13">
                                                        <li>
                                                            <a href="/"></a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='specialization-sublist-item-18_14']"
                                                       data-collapsed-label="
                                        Должностные преступления&nbsp;▿
                                    ">
                                                        Должностные преступления&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="specialization-sublist-item-18_14">
                                                        <li>
                                                            <a href="/halatnost-vrachej-policii-uchastkovogo-uchitelej-statya-293">Халатность</a>
                                                        </li>
                                                        <li>
                                                            <a href="/falsifikaciya-dokumentov-finansovoj-otchetnosti-organizacii-statya-327">Фальсификация
                                                                финансовых документов </a>
                                                        </li>
                                                        <li>
                                                            <a href="/oskorblenie-predstavitelya-vlasti-statya-319-zashchita-sud-advokat">Оскорбление
                                                                представителя власти</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>

                                <div class="drop-footer">
                                    <div class="row">
                                        <div class="col-12 col-md-4 d-none d-md-block">
                                            <div class="keywords-search">
                                                <span class="keywords-search__label">Поиск по ключевым словам:</span>
                                                <div class="keywords-search__row">
                                                    <form action="https://yandex.ru/search/site/" method="get"
                                                          target="_blank" accept-charset="utf-8">
                                                        <input type="hidden" name="searchid" value="2305210">
                                                        <input type="hidden" name="l10n" value="ru">
                                                        <input type="hidden" name="reqenc" value="">
                                                        <input type="submit" class="submit" value="Найти">
                                                        <div class="keywords-search__text-wrap">
                                                            <input type="search" name="text"
                                                                   class="text js-search-everything-input"
                                                                   placeholder="Поиск по любому слову...">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4">
                                            <div class="contact-info">
                                                <span class="contact-info__question">Не нашли нужную услугу?</span>
                                                <p>Оставьте номер телефона.<br>Наш специалист проконсультирует
                                                    вас бесплатно!</p>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4">
                                            <div class="callback-form">
                                                <span class="callback-form__label">Бесплатная консультация:</span>

                                                <form method="post" class="c-feedback-form js-feedback-form"
                                                      data-parsley-validate="" novalidate="">
                                                    <div class="callback-form__row">
                                                        <input type="button"
                                                               class="js-feedback-form__submit submit"
                                                               value="Отправить">
                                                        <div class="callback-form__text-wrap">
                                                            <input type="text"
                                                                   class="js-feedback-form__phone-input text phone-mask"
                                                                   placeholder="Ваш телефон"
                                                                   data-parsley-required=""
                                                                   data-parsley-phone="" im-insert="true">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="top-nav__item">
                            <a href="/services" class="top-nav__link has-drop">
                                <span>Услуги</span>
                            </a>
                            <div class="top-nav-drop top-nav-drop--services">
                                <div class="first-level-holder">
                                    <ul class="first-level">
                                        <li>
                                            <a href="#" class=" active ">
                        <span class="title" data-id="services-menu-item-0">
                            Консультация
                        </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                        <span class="title" data-id="services-menu-item-1">
                            Суд
                        </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                        <span class="title" data-id="services-menu-item-2">
                            Юристы
                        </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                        <span class="title" data-id="services-menu-item-3">
                            Юристы
                        </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                        <span class="title" data-id="services-menu-item-4">
                            Юридическая помощь
                        </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="">
                        <span class="title" data-id="services-menu-item-5">
                            Документальное сопровождение
                        </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="second-level-holder">
                                    <div class="second-level-services">
                                        <div class="drop-header">
                <span class="drop-header__title">
                    <span class="drop-header__title-icon">
                        <img class="lazy loaded" data-src="images/icon-service.png" alt=""
                             src="images/icon-service.png" data-was-processed="true">
                    </span>
                    <span class="drop-header__title-text popup_service_title">Услуги</span>
                </span>
                                            <div class="sort-search">
                                                <div class="easy-autocomplete" style="width: 684.547px;"><input
                                                        type="text" class="text js-search-service-input"
                                                        placeholder="Начните набирать название..."
                                                        id="eac-1970" autocomplete="off">
                                                    <div class="easy-autocomplete-container"
                                                         id="eac-container-eac-1970">
                                                        <ul></ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="services-menu-item-0">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/besplatnye-yuridicheskie-konsultacii">Бесплатная
                                                        юридическая консультация</a>
                                                </li>
                                                <li>
                                                    <a href="/yuridicheskaya-konsultaciya-spb">Юридическая
                                                        консультация СПб</a>
                                                </li>
                                                <li>
                                                    <a href="/besplatnyj-yurist-onlajn">Бесплатный юрист
                                                        онлайн</a>
                                                </li>
                                                <li>
                                                    <a href="/zadat-vopros-yuristu">Задать вопрос юристу
                                                        онлайн</a>
                                                </li>
                                                <li>
                                                    <a href="/semejnye-konsultacii-yurista-spb">Семейные
                                                        консультации юриста</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-konsultaciya-spb-online">Консультация
                                                        юриста</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="services-menu-item-1">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/uslugi-sud-pomoshch-yurista-spb">Услуги в суде</a>
                                                </li>
                                                <li>
                                                    <a href="/sostavlenie-sudebnyh-dokumentov">Составление
                                                        судебных документов</a>
                                                </li>
                                                <li>
                                                    <a href="/obespechenie-iska-sudom-pomoshch-yurista">Обеспечение
                                                        иска</a>
                                                </li>
                                                <li>
                                                    <a href="/voprosy-po-ispolnitelnomu-listu-pomoshch-yurista">Вопросы
                                                        по исполнительному листу</a>
                                                </li>
                                                <li>
                                                    <a href="/provedenie-ehkspertiz-pomoshch-yurista-spb">Проведение
                                                        экспертиз</a>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='service-sublist-item-1_5']"
                                                       data-collapsed-label="
										Представительство в судах всех районов СПб и ЛО&nbsp;▿
									">
                                                        Представительство в судах всех районов СПб и
                                                        ЛО&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="service-sublist-item-1_5">
                                                        <li>
                                                            <a href="/predstavitelstvo-rajonnye-sudy-spb-lo-yurist-advokat">Представительство
                                                                в судах всех районов СПб и ЛО</a>
                                                        </li>
                                                        <li>
                                                            <a href="/sud/vsevolozhskij-rajonnyj-sud-leningradskoj-oblasti-spb">Всеволожский
                                                                городской суд</a>
                                                        </li>
                                                        <li>
                                                            <a href="/sud/primorskij-rajonnyj-sud-sankt-peterburg">Приморский
                                                                районный суд СПб</a>
                                                        </li>
                                                        <li>
                                                            <a href="/sud/petrogradskij-rajonnyj-sud-sankt-peterburg">Петроградский
                                                                районный суд СПб</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='service-sublist-item-1_6']"
                                                       data-collapsed-label="
										Защита в апелляционной инстанции&nbsp;▿
									">
                                                        Защита в апелляционной инстанции&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="service-sublist-item-1_6">
                                                        <li>
                                                            <a href="/apellyaciya-zhaloba-v-arbitrazhnyj-sud">Апелляция</a>
                                                        </li>
                                                        <li>
                                                            <a href="/obzhalovanie-v-sude-pomoshch-yurista-apellyaciya-kassaciya-nadzornaya-zhaloba">Обжалование
                                                                в суде</a>
                                                        </li>
                                                        <li>
                                                            <a href="/neustojka-apellyaciya-konsultacii-besplatno-spb-moskva">Неустойка
                                                                в апелляции</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='service-sublist-item-1_7']"
                                                       data-collapsed-label="
										Работа в кассационной инстанции&nbsp;▿
									">
                                                        Работа в кассационной инстанции&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="service-sublist-item-1_7">
                                                        <li>
                                                            <a href="/osporit-otkaz-reshenie-suda-nalogovoj-gosorganov">Оспаривание
                                                                отказов госорганов</a>
                                                        </li>
                                                        <li>
                                                            <a href="/predstavitelstvo-arbitrazhnom-processe-sude">Представительство
                                                                в арбитражном суде</a>
                                                        </li>
                                                        <li>
                                                            <a href="/sud-s-bankom-po-kreditu-ipoteke-yuristy-banki-spb-moskva">Суд
                                                                с банком по кредиту</a>
                                                        </li>
                                                        <li>
                                                            <a href="/sud-chlenskim-vznosam-snt-vzyskanie-uplata">Суд
                                                                по членский взносам</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="/semejnye-dela-v-sude-pomoshch-yurista">Семейные
                                                        дела в суде</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="services-menu-item-2">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/besplatnaya-konsultaciya-yurista-kruglosutochno">Юристы
                                                        "ВЫСШЕЙ ИНСТАНЦИИ"</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-sankt-peterburg-spb">Юрист СПб</a>
                                                </li>
                                                <li>
                                                    <a href="/rajonnyj-yurist-spb">Районный юрист</a>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='service-sublist-item-2_3']"
                                                       data-collapsed-label="
										Районные юристы&nbsp;▿
									">
                                                        Районные юристы&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="service-sublist-item-2_3">
                                                        <li>
                                                            <a href="/yurist-vsevolozhskij-rajon-konsultaciya-besplatno">Юрист
                                                                во Всеволожском районе</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="/avtomobilnyj-yurist-konsultaciya-besplatno">Автомобильный
                                                        юрист</a>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='service-sublist-item-2_5']"
                                                       data-collapsed-label="
										Финансовый юрист&nbsp;▿
									">
                                                        Финансовый юрист&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="service-sublist-item-2_5">
                                                        <li>
                                                            <a href="/yurist-bankovskim-delam-voprosam-konsultaciya">Банковский
                                                                юрист</a>
                                                        </li>
                                                        <li>
                                                            <a href="/yurist-ipoteke-konsultaciya-pomoshch">Юрист
                                                                по ипотеке</a>
                                                        </li>
                                                        <li>
                                                            <a href="/kreditnyj-yurist-spory-konsultaciya">Кредитный
                                                                юрист</a>
                                                        </li>
                                                        <li>
                                                            <a href="/finansovyj-yurist">Финансовый юрист</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="/yurist-voennye-dela-voprosy-ipoteka-pensiya">Военный
                                                        юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/grazhdanskij-yurist-advokat-grazhdanskim-voprosam-sporam-konsultaciya">Гражданский
                                                        юрист</a>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='service-sublist-item-2_8']"
                                                       data-collapsed-label="
										Имущественный юрист&nbsp;▿
									">
                                                        Имущественный юрист&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="service-sublist-item-2_8">
                                                        <li>
                                                            <a href="/yuristy-imushchestvennym-delam-sporam-voprosam">Имущественный
                                                                юрист</a>
                                                        </li>
                                                        <li>
                                                            <a href="/yurist-po-nedvizhimosti-konsultaciya">Юрист
                                                                по недвижимости</a>
                                                        </li>
                                                        <li>
                                                            <a href="/yurist-sdelkam-soprovozhdenie-registraciya-priznanie-nedejstvitelnoj">Юрист
                                                                по сделкам</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="/yuristy-nalogovym-sporam-voprosam-konsultaciya">Налоговый
                                                        юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/migracionnyj-yurist-besplatnaya-konsultaciya-spb">Миграционный
                                                        юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-administrativnym-delam-sporam-pravonarusheniyam">Юрист
                                                        по административным делам</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-po-zhilishchnym-voprosam-v-vashem-rajone-advokat">Юрист
                                                        по жилищным вопросам</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-dolg-spb">Юрист по долгам</a>
                                                </li>
                                                <li>
                                                    <a href="/zemelnyj-yurist-voprosy-spory-uslugi">Юрист по
                                                        земельным вопросам</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-po-zashchite-prav-potrebitelej-pomoshch">Юрист
                                                        по защите прав потребителей</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-kvartirnym-voprosam-pokupka-prodazha-oformit">Юрист
                                                        по квартирным вопросам</a>
                                                </li>
                                                <li>
                                                    <a href="/nasledstvennyj-yurist-zaveshchaniya-priznanie-prava-nedostojnym-sud">Юрист
                                                        по наследству</a>
                                                </li>
                                                <li>
                                                    <a href="/pensionnyj-yurist-konsultaciya">Юрист по
                                                        пенсионном вопросам</a>
                                                </li>
                                                <li>
                                                    <a href="/trudovoj-yurist-spory-konsultacii-besplatno">Юрист
                                                        по трудовому праву</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-turizmu-pomoshch-spory-konsultaciya">Юрист
                                                        по туризму</a>
                                                </li>
                                                <li>
                                                    <a href="/yuristy-redkoj-specializacii-pomoshch-konsultaciya">Юрист
                                                        с редкой специализацией</a>
                                                </li>
                                                <li>
                                                    <a href="/biznes-yurist-soprovozhdenie-zashchita">Бизнес-юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/stroitelnyj-yurist-konsultacii">Юрист по
                                                        строительству</a>
                                                </li>
                                                <li>
                                                    <a href="/transportnyj-yurist-spory-pomoshch-konsultaciya-besplatno">Транспортный
                                                        юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/tamozhennyj-yurist-spory-sud-pomoshch-besplatno">Таможенный
                                                        юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/yurist-blokchejn-bitkoin-ico-kriptovalyuta">Юрист
                                                        по вопросам блокчейн</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="services-menu-item-3">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/grazhdanskij-advokat-kollegiya-uslugi-konsultaciya">Юристы
                                                        "ВЫСШЕЙ ИНСТАНЦИИ"</a>
                                                </li>
                                                <li class="c-service-navigation-item-dropdown">
                                                    <a href="#" data-toggle="collapse"
                                                       data-target=".c-service-navigation-item__sub-list.collapse[data-id='service-sublist-item-3_1']"
                                                       data-collapsed-label="
										Юрист в вашем районе&nbsp;▿
									">
                                                        Юрист в вашем районе&nbsp;<span>▿</span>
                                                    </a>
                                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse"
                                                        data-id="service-sublist-item-3_1">
                                                        <li>
                                                            <a href="/rajonnyj-advokat-kollegiya-uslugi-konsultaciya">Юрист
                                                                в вашем районе</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-sankt-peterburga-spb-kollegiya-konsultaciya-palata">Юрист
                                                                в Санкт-Петербурге</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-admiraltejskij-rajon-uslugi-pomoshch-spb">Юрист
                                                                в Адмиралтейском районе</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-vasileostrovskogo-rajona-pomoshch-spb#">Юрист
                                                                по Василеостровскому район</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokaty-vyborskij-rajon-kollegiya-konsultaciya-pomoshch">Юрист
                                                                Выборгского района</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-gatchina-sud-konsultacii-besplatno">Юрист
                                                                Гатчина</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-kalininskij-rajon-pomoshch-uslugi-konsultacii">Юрист
                                                                в Калининском районе</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-v-kirovskom-rajone-besplatno-spb">Юрист
                                                                в Кировском районе</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-v-kolpino-konsultacii-besplatno-spb">Юрист
                                                                в Колпино</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-krasnogvardejskij-rajon-sud-konsultacii-besplatno">Юрист
                                                                в Красногвардейском районе</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-nevskij-rajon-konsultacii-besplatno-spb">Юрист
                                                                в Невском районе</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-petrogradskij-rajon-sud-konsultaciya-besplatno">Юрист
                                                                в Петроградском районе</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokaty-petrodvorcovogo-rajona-sud-konsultaciya-spb">Юрист
                                                                в Петродворцовом районе</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-v-primorskom-rajone-yuridicheskaya-pomoshch">Юрист
                                                                в Приморском районе</a>
                                                        </li>
                                                        <li>
                                                            <a href="/advokat-frunzenskij-rajon-pomoshch-uslugi-besplatno">Юрист
                                                                во Фрунзенском районе</a>
                                                        </li>
                                                        <li>
                                                            <a href="/kollegiya-advokatov-centralnogo-rajona-sud-konsultaciya">Юрист
                                                                в Центральном районе</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="/advokat-sankt-peterburg-spb">Юрист в СПб</a>
                                                </li>
                                                <li>
                                                    <a href="/uslugi-advokata-spb">Услуги юриста</a>
                                                </li>
                                                <li>
                                                    <a href="/avtomobilnyj-advokat">Автомобильный юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/arbitrazhnyj-advokat-arbitrazhnye-dela">Арбитражный
                                                        юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/voennyj-advokat-yuridicheskaya-pomoshch-voennosluzhashchim">Юрист
                                                        по военным вопросам</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-dtp-pomoshch-avariyah-yurist-konsultacii">Юрист
                                                        по ДТП</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-po-kreditam-dolgam-yurist-po-ipoteke">Кредитный
                                                        юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/ipotechnyj-advokat-pomoshch-besplatno-spb">Ипотечный
                                                        юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/medicinskij-advokat-voprosy-spory-konsultaciya">Юрист
                                                        по медицинским вопросам</a>
                                                </li>
                                                <li>
                                                    <a href="/grazhdanskij-yurist-advokat-grazhdanskim-voprosam-sporam-konsultaciya">Гражданский
                                                        юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-nedvizhimosti-konsultaciya-besplatno">Юрист
                                                        по недвижимости</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-stroitelnye-dela-spory-voprosy-uslugi-sud">Юрист
                                                        по строительным вопросам</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-nasledstvo-prinyatie-oformlenie-vosstanovlenie">Юрист
                                                        по наследственному праву</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-sdelkam-priznanie-nedejstvitelnoj-soprovozhdenie">Юрист
                                                        по сделках</a>
                                                </li>
                                                <li>
                                                    <a href="/besplatnyj-trudovoj-advokat-spory-dela">Юрист по
                                                        трудовым спорам</a>
                                                </li>
                                                <li>
                                                    <a href="/zemelnyj-advokat-mezhevye-spory-snt-dnp">Юрист по
                                                        земельным спорам</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-imushchestvennym-sporam-delam-voprosam-konsultaciya">Юрист
                                                        по имущественным спорам</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-strahovym-voprosam-kompensaciya-osago-kasko-yuristy">Юрист
                                                        по страховым вопросам</a>
                                                </li>
                                                <li>
                                                    <a href="/besplatnyj-advokat-yuridicheskaya-pomoshch-onlajn-konsultant">Бесплатный
                                                        юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/ugolovnyj-advokat-uslugi-pomoshch">Уголовный
                                                        юрист</a>
                                                </li>
                                                <li>
                                                    <a href="/advokat-v-sude-besplatno">Юрист суд</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="services-menu-item-4">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/yuridicheskoe-soprovozhdenie-yuridicheskih-fizicheskih-lic">Юридическое
                                                        сопровождение от "ВЫСШЕЙ ИНСТАНЦИИ"</a>
                                                </li>
                                                <li>
                                                    <a href="/yuridicheskaya-firma-n1-v-sankt-peterburge">Юридическая
                                                        фирма "ВЫСШАЯ ИНСТАНЦИЯ"</a>
                                                </li>
                                                <li>
                                                    <a href="/yuridicheskaya-ocenka-spb-2018">Юридическая
                                                        оценка</a>
                                                </li>
                                                <li>
                                                    <a href="/podbor-arbitrazhnogo-upravlyayushchego">Подбор
                                                        арбитражного управляющего</a>
                                                </li>
                                                <li>
                                                    <a href="/yuridicheskie-uslugi">Юридические услуги</a>
                                                </li>
                                                <li>
                                                    <a href="/zashchita-prav-turistov-yurist-spb">Защита прав
                                                        туристов</a>
                                                </li>
                                                <li>
                                                    <a href="/yuridicheskie-uslugi-fizicheskim-licam">Юридические
                                                        услуги физическим лицам</a>
                                                </li>
                                                <li>
                                                    <a href="/yuridicheskie-uslugi-yuridicheskim-licam">Юридические
                                                        услуги юридическим лицам</a>
                                                </li>
                                                <li>
                                                    <a href="/yuridicheskaya-pomoshch">Юридическая помощь</a>
                                                </li>
                                                <li>
                                                    <a href="/uslugi-predstavitelya-sud-pomoshch-yursita">Услуги
                                                        представителя</a>
                                                </li>
                                                <li>
                                                    <a href="/ogranichenie-deesposobnosti-grazhdani">Ограничение
                                                        дееспособности гражданина</a>
                                                </li>
                                                <li>
                                                    <a href="/pomoshch-yurista-spb-onlajn">Помощь юриста</a>
                                                </li>
                                                <li>
                                                    <a href="/yuridicheskaya-zashchita">Юридическая защита</a>
                                                </li>
                                                <li>
                                                    <a href="/udalenie-negativnyh-otzyvov-kommentariev-stoimost">Удаление
                                                        негативных отзывов</a>
                                                </li>
                                                <li>
                                                    <a href="/rabota-otkazami-notariusa-zayavlenie-pomoshch-yurist">Работа
                                                        с отказами нотариуса</a>
                                                </li>
                                                <li>
                                                    <a href="/otkaz-v-zemle-pod-izhs-pomoshch-yuista">Отказ
                                                        предоставить землю</a>
                                                </li>
                                                <li>
                                                    <a href="/uslugi-po-vzyskaniyu">Услуги по взысканию</a>
                                                </li>
                                                <li>
                                                    <a href="/rozysk-imushchestva-pomoshch-yurista">Розыск
                                                        имущества</a>
                                                </li>
                                                <li>
                                                    <a href="/vzyskanie-nds-kontragenta-direktora">Взыскание
                                                        НДС</a>
                                                </li>
                                                <li>
                                                    <a href="/yuridicheskoe-obsluzhivanie-yuridicheskih-lic ">Юридическое
                                                        обслуживание юридических лиц</a>
                                                </li>
                                                <li>
                                                    <a href="/Oformlenie-sobstvennosti-doma-zemli-uchastka-nasledstva-sdelki-kvartiry">Оформление
                                                        собственности</a>
                                                </li>
                                                <li>
                                                    <a href="/rastorzhenie-dogovora-v-odnostoronnem-poryadke-pomoshch-yurista">Расторжение
                                                        договора в одностороннемпорядке</a>
                                                </li>
                                                <li>
                                                    <a href="/zashchita-prava-sobstvennosti-pomoshch-yurista">Защита
                                                        права собственности</a>
                                                </li>
                                                <li>
                                                    <a href="/izyatie-zemelnyh-uchastkov-zashchita-pomoshch-yurista">Защита
                                                        от изъятия земельных участков</a>
                                                </li>
                                                <li>
                                                    <a href="/uslugi-44-fz-pomoshch-yurista">Услуги по 44 ФЗ</a>
                                                </li>
                                                <li>
                                                    <a href="/yuridicheskoe-soprovozhdenie-tenderov-goszakupok">Сопровождение
                                                        тендеров и госзакупок</a>
                                                </li>
                                                <li>
                                                    <a href="/nekachestvennyj-remont">Некачественный ремонт</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="popup_service_list pretty-links-decorator"
                                             data-id="services-menu-item-5">
                                            <ul class="drop-list drop-list_two">
                                                <li>
                                                    <a href="/podgotovka-dokumentov-pomoshch-yurista-oformlenie-sud">Подготовка
                                                        любых документов</a>
                                                </li>
                                                <li>
                                                    <a href="/sostavlenie-dogovora-pomoshch-yurista">Составление
                                                        договоров</a>
                                                </li>
                                                <li>
                                                    <a href="/sostavlenie-zhalob-pomoshch-yurista">Составление
                                                        жалоб</a>
                                                </li>
                                                <li>
                                                    <a href="/uslugi-isk-pomoshch-yurista">Иск. Услуги</a>
                                                </li>
                                                <li>
                                                    <a href="/sostavlenie-podacha-osparivanie-iskovogo-zayavleniya-iska-yurist-advokat">Составление
                                                        искового заявления</a>
                                                </li>
                                                <li>
                                                    <a href="/dosudebnaya-pretenziya-pomoshch-yurista">Досудебная
                                                        претензия</a>
                                                </li>
                                                <li>
                                                    <a href="/registraciya-lyubyh-prav-pomoshch-yurista">Оформление
                                                        любых прав</a>
                                                </li>
                                                <li>
                                                    <a href=" /pomoshch-vkladchikam-semejnogo-kapitala">Помощь
                                                        вкладчикам семейного капитала</a>
                                                </li>
                                                <li>
                                                    <a href="/pereoformlenie-doma-pomoshch-yurista">Переоформление
                                                        дома</a>
                                                </li>
                                                <li>
                                                    <a href="/stoimost-dogovora-dareniya-darstvennoj">Договор
                                                        дарения (дарственная). Стоимость</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="keywords-search d-block d-md-none">
                                            <span class="keywords-search__label">Поиск по ключевым словам:</span>
                                            <div class="keywords-search__row">
                                                <form action="https://yandex.ru/search/site/" method="get"
                                                      target="_blank" accept-charset="utf-8">
                                                    <input type="hidden" name="searchid" value="2305210">
                                                    <input type="hidden" name="l10n" value="ru">
                                                    <input type="hidden" name="reqenc" value="">
                                                    <input type="submit" class="submit" value="Найти">
                                                    <div class="keywords-search__text-wrap">
                                                        <input type="search" name="text"
                                                               class="text js-search-everything-input"
                                                               placeholder="Поиск по любому слову...">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="drop-footer">
                                    <div class="row">
                                        <div class="col-12 col-md-4 d-none d-md-block">
                                            <div class="keywords-search">
                                                <span class="keywords-search__label">Поиск по ключевым словам:</span>
                                                <div class="keywords-search__row">
                                                    <form action="https://yandex.ru/search/site/" method="get"
                                                          target="_blank" accept-charset="utf-8">
                                                        <input type="hidden" name="searchid" value="2305210">
                                                        <input type="hidden" name="l10n" value="ru">
                                                        <input type="hidden" name="reqenc" value="">
                                                        <input type="submit" class="submit" value="Найти">
                                                        <div class="keywords-search__text-wrap">
                                                            <input type="search" name="text"
                                                                   class="text js-search-everything-input"
                                                                   placeholder="Поиск по любому слову...">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4">
                                            <div class="contact-info">
                                                <span class="contact-info__question">Не нашли нужную услугу?</span>
                                                <p>Оставьте номер телефона.<br>Наш специалист проконсультирует
                                                    вас бесплатно!</p>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4">
                                            <div class="callback-form">
                                                <span class="callback-form__label">Бесплатная консультация:</span>

                                                <form method="post" class="c-feedback-form js-feedback-form"
                                                      data-parsley-validate="" novalidate="">
                                                    <div class="callback-form__row">
                                                        <input type="button"
                                                               class="js-feedback-form__submit submit"
                                                               value="Отправить">
                                                        <div class="callback-form__text-wrap">
                                                            <input type="text"
                                                                   class="js-feedback-form__phone-input text phone-mask"
                                                                   placeholder="Ваш телефон"
                                                                   data-parsley-required=""
                                                                   data-parsley-phone="" im-insert="true">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="top-nav__item">
                            <a href="/contacts" class="top-nav__link">
                                <span>Контакты</span>
                            </a>
                        </li>
                    </ul>
                    <div class="top-nav-overlay js-top-nav-close"></div>
                </div>
            </div>
            <!-- /Desktop Navigation Bar -->

            <a href="tel:+78126135138" class="btn-call js-mango-number">8 (812) 613-51-38</a>
        </div>
    </div>
    <div class="top-info">
        <div class="c-top-navigation-bar top-info__holder">
            <div class="container">
                <div class="d-block d-md-none text-center mb-sm-3">
                    <a href="tel:88125646948" class="phone top-info__tel">8 (812) 564-69-48</a>
                </div>

                <div class="top-row">
                    <div class="top-row__left">
                        <div class="c-top-navigation-bar__item rating-info">
                            <div class="rating-info__score"><span>9.8</span> / 10</div>
                            <div class="rating-info__text">
                                Рейтинг
                                <div class="rating-info__popup-wrap">
                                    <a href="#" class="rating-info__popup-opener js-reviews-opener">по отзывам</a>
                                    <div class="rating-info__popup">
                                        <span class="rating-info__popup-label">Почитайте отзывы о нас:</span>
                                        <ul class="rating-info__popup-list">
                                            <li>
                                                <a href="https://sravniotzyvy.com" target="_blank"
                                                   rel="nofollow noopener noreferrer">
                                                    sravniotzyvy.com
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://reviewscompanies.com" target="_blank"
                                                   rel="nofollow noopener noreferrer">
                                                    reviewscompanies.com
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://blackotzyvy.info" target="_blank"
                                                   rel="nofollow noopener noreferrer">
                                                    blackotzyvy.info
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://grandreviews.org" target="_blank"
                                                   rel="nofollow noopener noreferrer">
                                                    grandreviews.org
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://nesovetyu.com" target="_blank"
                                                   rel="nofollow noopener noreferrer">
                                                    nesovetyu.com
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://nashemnenie.com" target="_blank"
                                                   rel="nofollow noopener noreferrer">
                                                    nashemnenie.com
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://antijob.biz" target="_blank"
                                                   rel="nofollow noopener noreferrer">
                                                    antijob.biz
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://commentjob.info" target="_blank"
                                                   rel="nofollow noopener noreferrer">
                                                    commentjob.info
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://whoisyourboss.club" target="_blank"
                                                   rel="nofollow noopener noreferrer">
                                                    whoisyourboss.club
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://orabote.club" target="_blank"
                                                   rel="nofollow noopener noreferrer">
                                                    orabote.club
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <span class="d-none d-md-in">на 02.12.2019</span>
                            </div>
                            <div class="rating-info__overlay js-reviews-close"></div>
                        </div>
                        <div class="c-top-navigation-bar__item mass-media">
                            <a href="#mass-modal" class="mass-media__opener fancybox">
                                <span class="mass-media__icon">
                                    <img class="lazy loaded" data-src="images/icon-media.png" alt="" src="images/icon-media.png" data-was-processed="true">
                                </span>
                                <span class="mass-media__text">Мы в СМИ</span>
                            </a>
                        </div>
                    </div>
                    <div class="top-row__right">
                        <div class="d-none d-sm-inline-flex">
                            <div class="offices-box">
                                <a href="#"
                                   class="offices-box__opener js-office-opener"><span>Офисы в СПб, ЛО</span></a>
                                <div class="offices-box__popup">
                                    <div class="offices-box__content">
                                        <div class="offices-box__offices-content">
                                            <span class="offices-box__content-title">Отделения</span>
                                            <ul class="offices-list popup_offices-list active" data-id="moscow">
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy loaded" data-src="images/icon-underground.png"
                                                             alt="" width="15" height="11"
                                                             src="images/icon-underground.png"
                                                             data-was-processed="true">
                                                    </div>
                                                    <div class="offices-list__text"><b>Комендантский проспект</b>, ул.
                                                        Гаккелевская, 21.
                                                        БЦ Балтийский Деловой Центр (Ресо)
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy loaded" data-src="images/icon-underground.png"
                                                             alt="" width="15" height="11"
                                                             src="images/icon-underground.png"
                                                             data-was-processed="true">
                                                    </div>
                                                    <div class="offices-list__text"><b>Ладожская</b>, Пл. Карла Фаберже
                                                        д. 8. БЦ Русские
                                                        Самоцветы
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy loaded" data-src="images/icon-underground.png"
                                                             alt="" width="15" height="11"
                                                             src="images/icon-underground.png"
                                                             data-was-processed="true">
                                                    </div>
                                                    <div class="offices-list__text"><b>Петроградская</b>, ул. Профессора
                                                        Попова 37 лит
                                                        Щ. БЦ Сенатор
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy loaded" data-src="images/icon-underground.png"
                                                             alt="" width="15" height="11"
                                                             src="images/icon-underground.png"
                                                             data-was-processed="true">
                                                    </div>
                                                    <div class="offices-list__text"><b>Площадь Александра Невского</b>,
                                                        ул. Херсонская
                                                        д. 12-14. БЦ Renaissance Pravda
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy loaded" data-src="images/icon-underground.png"
                                                             alt="" width="15" height="11"
                                                             src="images/icon-underground.png"
                                                             data-was-processed="true">
                                                    </div>
                                                    <div class="offices-list__text"><b>Площадь Ленина</b>, Пискарёвский
                                                        просп., 2. БЦ
                                                        Бенуа
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy loaded" data-src="images/icon-underground.png"
                                                             alt="" width="15" height="11"
                                                             src="images/icon-underground.png"
                                                             data-was-processed="true">
                                                    </div>
                                                    <div class="offices-list__text"><b>Пролетарская</b>, пр. Обуховской
                                                        обороны 112,
                                                        литера И, 3. БЦ Вант
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy loaded" data-src="images/icon-underground.png"
                                                             alt="" width="15" height="11"
                                                             src="images/icon-underground.png"
                                                             data-was-processed="true">
                                                    </div>
                                                    <div class="offices-list__text"><b>Сенная Площадь</b>, наб. р. Мойки
                                                        58, литера А.
                                                        БЦ Мариинский
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy loaded" data-src="images/icon-underground.png"
                                                             alt="" width="15" height="11"
                                                             src="images/icon-underground.png"
                                                             data-was-processed="true">
                                                    </div>
                                                    <div class="offices-list__text"><b>Технологический Институт</b>,
                                                        10-я
                                                        Красноармейская, 22. БЦ Kellermann Center
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy loaded" data-src="images/icon-underground.png"
                                                             alt="" width="15" height="11"
                                                             src="images/icon-underground.png"
                                                             data-was-processed="true">
                                                    </div>
                                                    <div class="offices-list__text"><b>Черная Речка</b>, Аптекарская
                                                        наб., д. 20. БЦ
                                                        Авеню
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy loaded" data-src="images/icon-underground.png"
                                                             alt="" width="15" height="11"
                                                             src="images/icon-underground.png"
                                                             data-was-processed="true">
                                                    </div>
                                                    <div class="offices-list__text"><b>Площадь Восстания</b>, Лиговский
                                                        проспект 21. БЦ
                                                        Сенатор
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul class="offices-list popup_offices-list" data-id="ek">
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Комендантский проспект1</b>, ул.
                                                        Гаккелевская,
                                                        21. БЦ Балтийский Деловой Центр (Ресо)
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Ладожская1</b>, Пл. Карла Фаберже
                                                        д. 8. БЦ
                                                        Русские Самоцветы
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Петроградская</b>, ул. Профессора
                                                        Попова 37 лит
                                                        Щ. БЦ Сенатор
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Площадь Александра Невского</b>,
                                                        ул. Херсонская
                                                        д. 12-14. БЦ Renaissance Pravda
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Площадь Ленина</b>, Пискарёвский
                                                        просп., 2. БЦ
                                                        Бенуа
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Пролетарская</b>, пр. Обуховской
                                                        обороны 112,
                                                        литера И, 3. БЦ Вант
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Сенная Площадь</b>, наб. р. Мойки
                                                        58, литера А.
                                                        БЦ Мариинский
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Технологический Институт</b>,
                                                        10-я
                                                        Красноармейская, 22. БЦ Kellermann Center
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Черная Речка</b>, Аптекарская
                                                        наб., д. 20. БЦ
                                                        Авеню
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Площадь Восстания</b>, Лиговский
                                                        проспект 21. БЦ
                                                        Сенатор
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul class="offices-list popup_offices-list" data-id="ufa">
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Комендантский проспект</b>, ул.
                                                        Гаккелевская, 21.
                                                        БЦ Балтийский Деловой Центр (Ресо)
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Ладожская</b>, Пл. Карла Фаберже
                                                        д. 8. БЦ Русские
                                                        Самоцветы
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Петроградская</b>, ул. Профессора
                                                        Попова 37 лит
                                                        Щ. БЦ Сенатор
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Площадь Александра Невского</b>,
                                                        ул. Херсонская
                                                        д. 12-14. БЦ Renaissance Pravda
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Площадь Ленина</b>, Пискарёвский
                                                        просп., 2. БЦ
                                                        Бенуа
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Пролетарская</b>, пр. Обуховской
                                                        обороны 112,
                                                        литера И, 3. БЦ Вант
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Сенная Площадь</b>, наб. р. Мойки
                                                        58, литера А.
                                                        БЦ Мариинский
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Технологический Институт</b>,
                                                        10-я
                                                        Красноармейская, 22. БЦ Kellermann Center
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Черная Речка</b>, Аптекарская
                                                        наб., д. 20. БЦ
                                                        Авеню
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Площадь Восстания</b>, Лиговский
                                                        проспект 21. БЦ
                                                        Сенатор
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul class="offices-list popup_offices-list" data-id="kr">
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Комендантский проспект</b>, ул.
                                                        Гаккелевская, 21.
                                                        БЦ Балтийский Деловой Центр (Ресо)
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Ладожская</b>, Пл. Карла Фаберже
                                                        д. 8. БЦ Русские
                                                        Самоцветы
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Петроградская</b>, ул. Профессора
                                                        Попова 37 лит
                                                        Щ. БЦ Сенатор
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Площадь Александра Невского</b>,
                                                        ул. Херсонская
                                                        д. 12-14. БЦ Renaissance Pravda
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Площадь Ленина</b>, Пискарёвский
                                                        просп., 2. БЦ
                                                        Бенуа
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Пролетарская</b>, пр. Обуховской
                                                        обороны 112,
                                                        литера И, 3. БЦ Вант
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Сенная Площадь</b>, наб. р. Мойки
                                                        58, литера А.
                                                        БЦ Мариинский
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Технологический Институт</b>,
                                                        10-я
                                                        Красноармейская, 22. БЦ Kellermann Center
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Черная Речка</b>, Аптекарская
                                                        наб., д. 20. БЦ
                                                        Авеню
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Площадь Восстания</b>, Лиговский
                                                        проспект 21. БЦ
                                                        Сенатор
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul class="offices-list popup_offices-list" data-id="kazan">
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Комендантский проспект</b>, ул.
                                                        Гаккелевская, 21.
                                                        БЦ Балтийский Деловой Центр (Ресо)
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Ладожская</b>, Пл. Карла Фаберже
                                                        д. 8. БЦ Русские
                                                        Самоцветы
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img class="lazy" data-src="images/icon-underground.png" alt=""
                                                             width="15" height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Петроградская</b>, ул. Профессора
                                                        Попова 37 лит
                                                        Щ. БЦ Сенатор
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img src="images/icon-underground.png" alt="" width="15"
                                                             height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Площадь Александра Невского</b>,
                                                        ул. Херсонская
                                                        д. 12-14. БЦ Renaissance Pravda
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img src="images/icon-underground.png" alt="" width="15"
                                                             height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Площадь Ленина</b>, Пискарёвский
                                                        просп., 2. БЦ
                                                        Бенуа
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img src="images/icon-underground.png" alt="" width="15"
                                                             height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Пролетарская</b>, пр. Обуховской
                                                        обороны 112,
                                                        литера И, 3. БЦ Вант
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img src="images/icon-underground.png" alt="" width="15"
                                                             height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Сенная Площадь</b>, наб. р. Мойки
                                                        58, литера А.
                                                        БЦ Мариинский
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img src="images/icon-underground.png" alt="" width="15"
                                                             height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Технологический Институт</b>,
                                                        10-я
                                                        Красноармейская, 22. БЦ Kellermann Center
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img src="images/icon-underground.png" alt="" width="15"
                                                             height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Черная Речка</b>, Аптекарская
                                                        наб., д. 20. БЦ
                                                        Авеню
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img src="images/icon-underground.png" alt="" width="15"
                                                             height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Площадь Восстания</b>, Лиговский
                                                        проспект 21. БЦ
                                                        Сенатор
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul class="offices-list popup_offices-list" data-id="nov">
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img src="images/icon-underground.png" alt="" width="15"
                                                             height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Комендантский проспект</b>, ул.
                                                        Гаккелевская, 21.
                                                        БЦ Балтийский Деловой Центр (Ресо)
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img src="images/icon-underground.png" alt="" width="15"
                                                             height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Ладожская</b>, Пл. Карла Фаберже
                                                        д. 8. БЦ Русские
                                                        Самоцветы
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img src="images/icon-underground.png" alt="" width="15"
                                                             height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Петроградская</b>, ул. Профессора
                                                        Попова 37 лит
                                                        Щ. БЦ Сенатор
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img src="images/icon-underground.png" alt="" width="15"
                                                             height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Площадь Александра Невского</b>,
                                                        ул. Херсонская
                                                        д. 12-14. БЦ Renaissance Pravda
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img src="images/icon-underground.png" alt="" width="15"
                                                             height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Площадь Ленина</b>, Пискарёвский
                                                        просп., 2. БЦ
                                                        Бенуа
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img src="images/icon-underground.png" alt="" width="15"
                                                             height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Пролетарская</b>, пр. Обуховской
                                                        обороны 112,
                                                        литера И, 3. БЦ Вант
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img src="images/icon-underground.png" alt="" width="15"
                                                             height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Сенная Площадь</b>, наб. р. Мойки
                                                        58, литера А.
                                                        БЦ Мариинский
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img src="images/icon-underground.png" alt="" width="15"
                                                             height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Технологический Институт</b>,
                                                        10-я
                                                        Красноармейская, 22. БЦ Kellermann Center
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img src="images/icon-underground.png" alt="" width="15"
                                                             height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Черная Речка</b>, Аптекарская
                                                        наб., д. 20. БЦ
                                                        Авеню
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="offices-list__icon">
                                                        <img src="images/icon-underground.png" alt="" width="15"
                                                             height="11">
                                                    </div>
                                                    <div class="offices-list__text"><b>Площадь Восстания</b>, Лиговский
                                                        проспект 21. БЦ
                                                        Сенатор
                                                    </div>
                                                </li>
                                            </ul>

                                        </div>
                                        <div class="offices-box__offices-zones">

                                        </div>
                                    </div>

                                    <div class="offices-box__sidebar">
                                        <div class="main-office">
                                            <span class="main-office__sub-title">Центральный филиал в<br>Санкт-Петербурге</span>
                                            <address class="main-office__address">Лиговский пр. дом 21</address>
                                            <a href="#" class="offices-box__content-opener js-sub-office-opener"><span>Отделения</span></a>
                                            <div class="main-office__schedule">
                                                <span class="main-office__schedule-label">Режим работы офиса:</span>
                                                <span class="main-office__schedule-time">Пн. - Пт.: </span>
                                                <span class="main-office__schedule-time">Суббота: </span>
                                            </div>
                                            <div class="main-office__schedule">
                                                <span class="main-office__schedule-label">Телефон:</span>
                                                <span class="main-office__schedule-time">
                        <a href="tel:+78127482361" class="phone">
                            <b>8 (812) 748-23-61</b>
                        </a>
                    </span>
                                            </div>

                                            <span class="main-office__schedule-ordering">Заключение договоров:<span>24/7</span></span>
                                            <hr>
                                            <p>
                                                Вы можете <a href="#" class="copy_contact">скопировать наши контакты в
                                                    буфер обмена</a>.
                                            </p>
                                            <div class="copy_message">Контакты скопированы в буфер обмена.</div>
                                        </div>
                                    </div>
                                    <a href="#"
                                       class="offices-box__popup-close js-sub-office-close"><span>Свернуть</span></a>
                                </div>
                                <div class="offices-box__overlay js-office-close"></div>
                            </div>

                            <a href="tel:+78005006696" class="top-info__phone d-none d-md-block mr-3">
                                8 (800) 500-66-96
                            </a>
                            <a href="tel:+78126135138" class="top-info__phone d-none d-md-block js-mango-number">8 (812)
                                613-51-38</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</header>