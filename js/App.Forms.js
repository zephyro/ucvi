
function submitFeedbackForm(formSubmissionId, data, url) {
    console.debug(data);

    if (data === null)
        return;

    return $.ajax({
        url: url,
        type: 'POST',
        data: data
    }).done(function (data) {
        console.log('API has successfully received inquiry');

        window.app.EventBus.$emit('after_form_submit', formSubmissionId, data);
    });
}

function initFeedbackForm(settings) {
    var o = Object.assign({}, {
        formElement: null,
        phoneInputSelector: '.js-feedback-form__phone-input',
        dateInputSelector: '.js-feedback-form__date-input',
        timeInputSelector: '.js-feedback-form__time-input',
        submitButtonSelector: '.js-feedback-form__submit',
        isValid: function () {
            return this.formElement.parsley().isValid();
        },
        hasDateTime: function() {
            return this.formElement.hasClass('js-feedback-form--with-time');
        },
        getPhoneValue: function() {
            var value = this.formElement.find(this.phoneInputSelector).inputmask('unmaskedvalue');

            if (value != null)
                value = "7".concat(value);

            return value;
        },
        getDateValue: function() {
            var value = this.formElement.find(this.dateInputSelector).inputmask('unmaskedvalue');

            return value;
        },
        getTimeValue: function() {
            var value = this.formElement.find(this.timeInputSelector).val();

            return value;
        },
        getFormSubmissionId: function() {
            var result = this.formElement.data('form-submission-id');

            if (result === null)
                console.warn("Форма не имеет атрибута data-submission-id");

            return result;
        },
        getFormData: function() {
            var result = {
                phone: this.getPhoneValue()
            };

            if (this.hasDateTime()) {
                result["date"] = this.getDateValue();

                var time =  this.getTimeValue();

                if (!time.length)
                    time = null;

                result["time"] = time;
            }

            return result;
        },
        onBeginSubmit: function() {
            this.formElement.find(this.phoneInputSelector).prop('disabled', true);
            this.formElement.find(this.submitButtonSelector).prop('disabled', true);
        },
        onEndSubmit: function() {
        },
        submit: function () {
            console.debug("FORM SUBMIT TRIGGERED");

            var formElement = o.formElement;

            if (!formElement.length)
                return;

            var parsleyForm = formElement.parsley();
            parsleyForm.validate();

            var self = this;

            parsleyForm.whenValid().then(function() {
                self.onBeginSubmit();

                var url = formElement.data('url');

                if (url === undefined)
                    url = window.config.api_base_url + '/api/web/forms/callback';

                submitFeedbackForm(self.getFormSubmissionId(), self.getFormData(), url).done(function () {
                    self.onEndSubmit();
                });
            });

        },
        onComplete: function () {
            this.submit();
        },
    }, settings);

    o.formElement.find(o.phoneInputSelector).inputmask({
        mask: "+7 (999) 999-99-99",
        autoUnmask: true,
        oncomplete: function () {
            o.submit();
        }
    });

    o.formElement.find(o.dateInputSelector).datepicker({
        dateFormat: "dd" + "." + "mm" + "." + "yyyy"
    }).inputmask({
        alias: "datetime",
        inputFormat: "dd.mm.yyyy",
        placeholder: 'дд.мм.гггг'
    });

    o.formElement.find(o.timeInputSelector).inputmask({
        mask: "99:99",
        autoUnmask: false
    });

    o.formElement.find(o.submitButtonSelector).click(function (e) {
        e.preventDefault();

        o.submit();
    });

    return o;
}

$(document).ready(function () {
    window.Parsley.addValidator('phone', {
        validateString: function(value) {
            console.debug("[PARSLEY] PHONE VALIDATOR TRIGGERED ON ", value);

            var m = value.match(/\d{3}\d{3}\d{2}\d{2}/);

            return m != null;
        }
    });

    $('.js-feedback-form').each(function (index, element) {
        initFeedbackForm({
            formElement: $(element)
        });
    });
});
