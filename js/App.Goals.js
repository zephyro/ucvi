window["Analytics"] = {
    getYandexCounter: function () {
        return window["yaCounter" + window.config["yandex_metrika_counter_id"]];
    },

    triggerCallbackGoal: function () {
        var self = this;

        self.safeCall(function () {
            gtag('event', 'CALLBACK_REQUESTED');
        });

        self.safeCall(function () {
            self.getYandexCounter().reachGoal('CALLBACK_REQUESTED');
            self.getYandexCounter().reachGoal('callMango'); // временно (удалить после теста новых целей)
        });
    },

    safeCall: function (callback) {
        if (callback) {
            try {
                return callback();
            }
            catch (e) {
                console.error(e);
            }
        }

        return null;
    }
};

window.app.EventBus.$on('after_form_submit', function (formSubmissionId, formData) {
    var analytics = window.Analytics;

    switch (formSubmissionId) {
        case "send_contacts_via_sms": {
            analytics.safeCall(function () {
                gtag('event', 'CONTACTS_REQUESTED_VIA_SMS');
            });

            analytics.safeCall(function () {
                analytics.getYandexCounter().reachGoal('CONTACTS_REQUESTED_VIA_SMS');
                analytics.getYandexCounter().reachGoal('querySMS'); // временно (удалить после теста новых целей)
            });
        } break;

        case "top_callback_form": {
            analytics.safeCall(function () {
                gtag('event', 'CALLBACK_REQUESTED_IN_HEADER');
            });

            analytics.safeCall(function () {
                analytics.getYandexCounter().reachGoal('CALLBACK_REQUESTED_IN_HEADER');
                analytics.getYandexCounter().reachGoal('upFormCallback'); // временно (удалить после теста новых целей)
            });

            analytics.triggerCallbackGoal();
        } break;

        case "deferred_callback_form": {
            analytics.safeCall(function () {
                gtag('event', 'DEFERRED_CALLBACK_REQUESTED');
            });

            analytics.safeCall(function () {
                analytics.getYandexCounter().reachGoal('DEFERRED_CALLBACK_REQUESTED');
            });

            analytics.triggerCallbackGoal();
        } break;

        default: {
            analytics.triggerCallbackGoal();
        }
    }
});
