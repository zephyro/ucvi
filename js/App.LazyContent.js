window.lazyContentManager.registerHandlers(['need_to_know'], function (id, $element) {

});

window.lazyContentManager.registerHandler('how_it_works', function (id, $element) {
    applyGlobalScriptsForElement($element);
    initAccordion($element);
    initTabs($element);

    $element.find('.js-how-it-works__open-at-load').click();
});

window.lazyContentManager.registerHandler('consultation', function (id, $element) {
    applyGlobalScriptsForElement($element);
    initAccordion($element);
    initTabs($element);

    $element.find('.js-feedback-form').each(function (index, element) {
        initFeedbackForm({
            formElement: $(element)
        });
    });
});

window.lazyContentManager.registerHandler('team', function (id, $element) {
    applyGlobalScriptsForElement($element);
    initTabs($element);

    var owlTeam = $element.find('.team-slider').owlCarousel({
        startPosition: 0,
        items: 4,
        margin: 30,
        stagePadding: 0,
        smartSpeed: 450,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            800: {
                items: 2
            },
            1000: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    });
    owlTeam.owlCarousel();
    $element.find('.team-next').click(function(e) {
        e.preventDefault();
        owlTeam.trigger('next.owl.carousel');
    });
    $element.find('.team-prev').click(function(e) {
        e.preventDefault();
        owlTeam.trigger('prev.owl.carousel');
    });
    var owlTeam2 = $element.find('.team-slider2').owlCarousel({
        startPosition: 0,
        items: 4,
        margin: 30,
        stagePadding: 0,
        smartSpeed: 450,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            800: {
                items: 2
            },
            1000: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    });
    owlTeam2.owlCarousel();
    $element.find('.team-next2').click(function(e) {
        e.preventDefault();
        owlTeam2.trigger('next.owl.carousel');
    });
    $element.find('.team-prev2').click(function(e) {
        e.preventDefault();
        owlTeam2.trigger('prev.owl.carousel');
    });
});

window.lazyContentManager.registerHandler('cases', function (id, $element) {
    applyGlobalScriptsForElement($element);
    initTabs($element);

    var owlCases = $element.find('.cases-slider').owlCarousel({
        startPosition: 0,
        items: 2,
        margin: 30,
        stagePadding: 0,
        slideBy: 2,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            1000: {
                items: 2
            }
        }
    });
    owlCases.owlCarousel();
    $element.find('.cases-next').click(function(e) {
        e.preventDefault();
        owlCases.trigger('next.owl.carousel');
    });
    $element.find('.cases-prev').click(function(e) {
        e.preventDefault();
        owlCases.trigger('prev.owl.carousel');
    });
});

window.lazyContentManager.registerHandlers(['mass_media', 'mass_media_modal'], function (id, $element) {
    applyGlobalScriptsForElement($element);
    initTabs($element);

    var owlMass2 = $element.find('.mass-slider2').owlCarousel({
        startPosition: 0,
        items: 2,
        margin: 30,
        stagePadding: 0,
        smartSpeed: 450,
        slideBy: 2,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            1000: {
                items: 2
            }
        }
    });

    owlMass2.owlCarousel();

    $element.find('.mass-next2').click(function (e) {
        e.preventDefault();
        owlMass2.trigger('next.owl.carousel');
    });

    $element.find('.mass-prev2').click(function (e) {
        e.preventDefault();
        owlMass2.trigger('prev.owl.carousel');
    });

    var owlMass3 = $element.find('.mass-slider3').owlCarousel({
        startPosition: 0,
        items: 2,
        margin: 0,
        stagePadding: 0,
        smartSpeed: 450,
        slideBy: 2,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            1000: {
                items: 2
            }
        }
    });

    owlMass3.owlCarousel();

    $element.find('.mass-next3').click(function (e) {
        e.preventDefault();
        owlMass3.trigger('next.owl.carousel');
    });

    $element.find('.mass-prev3').click(function (e) {
        e.preventDefault();
        owlMass3.trigger('prev.owl.carousel');
    });
});

window.lazyContentManager.registerHandler('certificates', function (id, $element) {
    initCertificatesSections($element);
});

window.lazyContentManager.registerHandler('reviews', function (id, $element) {
    applyGlobalScriptsForElement($element);
    initTabs($element);

    var carouselOptions = {
        startPosition: 0,
        items: 3,
        margin: 0,
        stagePadding: 0,
        smartSpeed: 450,
        slideBy: 2,
        loop: false,
        responsive: {
            0: {
                items: 1
            },
            900: {
                items: 2
            },
            1200: {
                items: 3
            }
        }
    };

    var owlReview1 = $element.find('.reviews-slider-1').owlCarousel(
        Object.assign({}, carouselOptions, {
            slideBy: 3
        })
    );

    owlReview1.owlCarousel();

    $element.find('.reviews-next1').click(function(e) {
        e.preventDefault();
        owlReview1.trigger('next.owl.carousel');
    });

    $element.find('.reviews-prev1').click(function(e) {
        e.preventDefault();
        owlReview1.trigger('prev.owl.carousel');
    });

    var owlReview2 = $element.find('.reviews-slider-2').owlCarousel(
        Object.assign({}, carouselOptions, {
            slideBy: 2
        })
    );

    owlReview2.owlCarousel();

    $element.find('.reviews-next2').click(function(e) {
        e.preventDefault();
        owlReview2.trigger('next.owl.carousel');
    });

    $element.find('.reviews-prev2').click(function(e) {
        e.preventDefault();
        owlReview2.trigger('prev.owl.carousel');
    });

    var owlReview3 = $element.find('.reviews-slider-3').owlCarousel(
        Object.assign({}, carouselOptions, {
            slideBy: 2
        })
    );

    owlReview3.owlCarousel();

    $element.find('.reviews-next3').click(function(e) {
        e.preventDefault();
        owlReview3.trigger('next.owl.carousel');
    });

    $element.find('.reviews-prev3').click(function(e) {
        e.preventDefault();
        owlReview3.trigger('prev.owl.carousel');
    });

    var owlReview4 = $element.find('.reviews-slider-4').owlCarousel(
        Object.assign({}, carouselOptions, {
            slideBy: 3
        })
    );

    owlReview4.owlCarousel();

    $element.find('.reviews-next4').click(function(e) {
        e.preventDefault();
        owlReview4.trigger('next.owl.carousel');
    });

    $element.find('.reviews-prev4').click(function(e) {
        e.preventDefault();
        owlReview4.trigger('prev.owl.carousel');
    });
});

window.lazyContentManager.registerHandler('prices', function (id, $element) {
    applyGlobalScriptsForElement($element);
    initTabs($element);
});

window.lazyContentManager.registerHandler('service_prices', function (id, $element) {
    applyGlobalScriptsForElement($element);
    initTabs($element);

    $element.find('.js-feedback-form').each(function (index, element) {
        initFeedbackForm({
            formElement: $(element)
        });
    });

    $element.find('.accordion').slideAccordion({
        opener: 'a.info-list__opener',
        slider: 'div.info-list__slide',
        collapsible: true,
        animSpeed: 300
    });
});

window.lazyContentManager.registerHandler('example', function (id, $element) {

});

// ---

window.currentPageId = null;
window.lazyContentManager.url = window.config["api_content_loader_url"];

var lazyContainers = document.querySelectorAll(".js-lazy-content-loader");

function observeLazyContent() {
    $(lazyContainers).each(function () {
        var $element = $(this);

        if ($element.isInViewport()) {
            window.lazyContentManager.activate($element, {
                page_id: window.currentPageId
            })
        }
    });
}

$(window).on('resize scroll', observeLazyContent);

$(document).on('afterShow.fb', function( e, instance, slide ) {
    observeLazyContent();
});
