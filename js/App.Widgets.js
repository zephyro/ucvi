jQuery(document).ready(function () {
    var analytics = window.Analytics;

    window.ws_OnChatVisitorIntroduced = function () {
        console.info("ws_OnChatVisitorIntroduced");

        analytics.safeCall(function () {
            gtag('event', 'ENVYBOX_VISITOR_INTRODUCED');
        });

        analytics.safeCall(function () {
            analytics.getYandexCounter().reachGoal('ENVYBOX_VISITOR_INTRODUCED');
        });
    };

    var callbackHandler = function () {
        analytics.safeCall(function () {
            gtag('event', 'ENVYBOX_CALL_REQUESTED');
        });

        analytics.safeCall(function () {
            analytics.getYandexCounter().reachGoal('ENVYBOX_CALL_REQUESTED');
        });
    };

    window.ws_OnCallbackOnlineCall = function () {
        console.info("ws_OnCallbackOnlineCall");

        callbackHandler();
    };

    window.ws_OnCallbackDeferredCall = function () {
        console.info("ws_OnCallbackDeferredCall");

        callbackHandler();
    };
});
