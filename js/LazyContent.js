
window.lazyContentManager.registerHandler('how_it_works', function (id, $element) {
    applyGlobalScriptsForElement($element);
    initAccordion($element);
    initTabs($element);

    var a2 = document.querySelector('#how-it-works__aside'), b2 = null, P2 = 220;

    if (a2) {
        window.addEventListener('scroll', Ascroll2, false);
        document.body.addEventListener('scroll', Ascroll2, false);

        function Ascroll2() {
            if (b2 == null) {
                var Sa = getComputedStyle(a2, ''), s = '';
                for (var i = 0; i < Sa.length; i++) {
                    if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                        s += Sa[i] + ': ' + Sa.getPropertyValue(Sa[i]) + '; '
                    }
                }
                b2 = document.createElement('div');
                b2.style.cssText = s + ' box-sizing: border-box; width: ' + a2.offsetWidth + 'px;';
                a2.insertBefore(b2, a2.firstChild);
                var l = a2.childNodes.length;
                for (var i = 2; i < l; i++) {
                    b2.appendChild(a2.childNodes[2]);
                }
                a2.style.height = b2.getBoundingClientRect().height + 'px';
                a2.style.padding = '0';
                a2.style.border = '0';
            }
            var Ra = a2.getBoundingClientRect(),
                R = Math.round(Ra.top + b2.getBoundingClientRect().height - document.querySelector('#how-it-works__content').getBoundingClientRect().bottom);  // селектор блока, при достижении нижнего края которого нужно открепить прилипающий элемент
            if ((Ra.top - P2) <= 0) {
                if ((Ra.top - P2) <= R) {
                    b2.className = 'stop';
                    b2.style.top = -R + 'px';
                } else {
                    b2.className = 'sticky';
                    b2.style.top = P2 + 'px';
                }
            } else {
                b2.className = '';
                b2.style.top = '';
            }
            window.addEventListener('resize', function () {
                a2.children[0].style.width = getComputedStyle(a2, '').width
            }, false);
        }
    }
});

window.lazyContentManager.registerHandler('team', function (id, $element) {
    applyGlobalScriptsForElement($element);
    initTabs($element);

    var owlTeam = $element.find('.team-slider').owlCarousel({
        startPosition: 0,
        items: 4,
        margin: 30,
        stagePadding: 0,
        smartSpeed: 450,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            800: {
                items: 2
            },
            1000: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    });
    owlTeam.owlCarousel();
    $element.find('.team-next').click(function(e) {
        e.preventDefault();
        owlTeam.trigger('next.owl.carousel');
    });
    $element.find('.team-prev').click(function(e) {
        e.preventDefault();
        owlTeam.trigger('prev.owl.carousel');
    });
    var owlTeam2 = $element.find('.team-slider2').owlCarousel({
        startPosition: 0,
        items: 4,
        margin: 30,
        stagePadding: 0,
        smartSpeed: 450,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            800: {
                items: 2
            },
            1000: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    });
    owlTeam2.owlCarousel();
    $element.find('.team-next2').click(function(e) {
        e.preventDefault();
        owlTeam2.trigger('next.owl.carousel');
    });
    $element.find('.team-prev2').click(function(e) {
        e.preventDefault();
        owlTeam2.trigger('prev.owl.carousel');
    });
});

window.lazyContentManager.registerHandler('cases', function (id, $element) {
    applyGlobalScriptsForElement($element);
    initTabs($element);

    var owlCases = $element.find('.cases-slider').owlCarousel({
        startPosition: 0,
        items: 2,
        margin: 30,
        stagePadding: 0,
        slideBy: 2,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            1000: {
                items: 2
            }
        }
    });
    owlCases.owlCarousel();
    $element.find('.cases-next').click(function(e) {
        e.preventDefault();
        owlCases.trigger('next.owl.carousel');
    });
    $element.find('.cases-prev').click(function(e) {
        e.preventDefault();
        owlCases.trigger('prev.owl.carousel');
    });
});

window.lazyContentManager.registerHandlers(['mass_media', 'mass_media_modal'], function (id, $element) {
    applyGlobalScriptsForElement($element);
    initTabs($element);

    var owlMass2 = $element.find('.mass-slider2').owlCarousel({
        startPosition: 0,
        items: 2,
        margin: 30,
        stagePadding: 0,
        smartSpeed: 450,
        slideBy: 2,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            1000: {
                items: 2
            }
        }
    });

    owlMass2.owlCarousel();

    $element.find('.mass-next2').click(function (e) {
        e.preventDefault();
        owlMass2.trigger('next.owl.carousel');
    });

    $element.find('.mass-prev2').click(function (e) {
        e.preventDefault();
        owlMass2.trigger('prev.owl.carousel');
    });

    var owlMass3 = $element.find('.mass-slider3').owlCarousel({
        startPosition: 0,
        items: 2,
        margin: 0,
        stagePadding: 0,
        smartSpeed: 450,
        slideBy: 2,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            1000: {
                items: 2
            }
        }
    });

    owlMass3.owlCarousel();

    $element.find('.mass-next3').click(function (e) {
        e.preventDefault();
        owlMass3.trigger('next.owl.carousel');
    });

    $element.find('.mass-prev3').click(function (e) {
        e.preventDefault();
        owlMass3.trigger('prev.owl.carousel');
    });
});

window.lazyContentManager.registerHandler('certificates', function (id, $element) {
    initCertificatesSections($element);
});

window.lazyContentManager.registerHandler('reviews', function (id, $element) {
    applyGlobalScriptsForElement($element);
    initTabs($element);

    var carouselOptions = {
        startPosition: 0,
        items: 3,
        margin: 0,
        stagePadding: 0,
        smartSpeed: 450,
        slideBy: 2,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            900: {
                items: 2
            },
            1200: {
                items: 3
            }
        }
    };

    var owlReview1 = $element.find('.reviews-slider-1').owlCarousel(
        Object.assign({}, carouselOptions, {
            slideBy: 3
        })
    );

    owlReview1.owlCarousel();

    $element.find('.reviews-next1').click(function(e) {
        e.preventDefault();
        owlReview1.trigger('next.owl.carousel');
    });
    $element.find('.reviews-prev1').click(function(e) {
        e.preventDefault();
        owlReview1.trigger('prev.owl.carousel');
    });
    var owlReview2 = $element.find('.reviews-slider-2').owlCarousel({
        startPosition: 0,
        items: 3,
        margin: 0,
        stagePadding: 0,
        smartSpeed: 450,
        slideBy: 2,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            900: {
                items: 2
            },
            1200: {
                items: 3
            }
        }
    });
    owlReview2.owlCarousel();
    $element.find('.reviews-next2').click(function(e) {
        e.preventDefault();
        owlReview2.trigger('next.owl.carousel');
    });
    $element.find('.reviews-prev2').click(function(e) {
        e.preventDefault();
        owlReview2.trigger('prev.owl.carousel');
    });
    var owlReview3 = $element.find('.reviews-slider-3').owlCarousel({
        startPosition: 0,
        items: 3,
        margin: 0,
        stagePadding: 0,
        smartSpeed: 450,
        slideBy: 2,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            900: {
                items: 2
            },
            1200: {
                items: 3
            }
        }
    });
    owlReview3.owlCarousel();
    $element.find('.reviews-next3').click(function(e) {
        e.preventDefault();
        owlReview3.trigger('next.owl.carousel');
    });
    $element.find('.reviews-prev3').click(function(e) {
        e.preventDefault();
        owlReview3.trigger('prev.owl.carousel');
    });
    var owlReview4 = $element.find('.reviews-slider-4').owlCarousel({
        startPosition: 0,
        items: 3,
        margin: 0,
        stagePadding: 0,
        smartSpeed: 450,
        slideBy: 3,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            900: {
                items: 2
            },
            1200: {
                items: 3
            }
        }
    });
    owlReview4.owlCarousel();
    $element.find('.reviews-next4').click(function(e) {
        e.preventDefault();
        owlReview4.trigger('next.owl.carousel');
    });
    $element.find('.reviews-prev4').click(function(e) {
        e.preventDefault();
        owlReview4.trigger('prev.owl.carousel');
    });
});

window.lazyContentManager.registerHandler('prices', function (id, $element) {
    applyGlobalScriptsForElement($element);
    initTabs($element);
});

window.lazyContentManager.registerHandler('service_prices', function (id, $element) {
    applyGlobalScriptsForElement($element);
    initTabs($element);

    console.log("accordeons", $element.find('.accordion'));

    $element.find('.accordion').slideAccordion({
        opener: 'a.info-list__opener',
        slider: 'div.info-list__slide',
        collapsible: true,
        animSpeed: 300
    });
});

window.lazyContentManager.registerHandler('example', function (id, $element) {

});
