function applyGlobalScriptsForElement(element) {
    var $element = $(element);

    function addParentBackground() {
        $('.bg-image').each(function() {
            if (typeof $(this).find('img').attr('src') != 'undefined') {
                $(this).css('background-image', 'url(' + $(this).find('img').attr('src') + ')');
            }
        });
    }
    addParentBackground();

    $element.find(".fancybox").fancybox({
        touch: false,
        focus: false,
        margin: [50, 0, 0, 50],
        afterShow: function () {

        },
    });

    $element.find('a.anchor[href^="#"]').click(function() {
        $('html, body').animate({
            scrollTop: $('[id="' + $.attr(this, 'href').substr(1) + '"]').offset().top - 40
        }, 600);
        return false;
    });

    new LazyLoad({
        elements_selector: ".lazy"
    });

    $element.find('.js-scroll-to-element').each(function(index, element) {
        initScrollToElementButton($(element));
    });

    $element.find('a.js-likbez-anchor').each(function(index, element) {
        initScrollToElementButton($(element));
    });
}

function initAccordion($element) {
    $element.find('.info-list').slideAccordion({
        opener: '>a.info-list__opener',
        slider: '>div.info-list__slide',
        collapsible: true,
        animSpeed: 300
    });

    $element.find('.steps-list').slideAccordion({
        opener: 'a.steps-list__opener',
        slider: '>div.steps-list__slide',
        collapsible: true,
        animSpeed: 300,
        allowMultiple: true
    });

    $element.find('.risks-accordion').slideAccordion({
        opener: '>a.risks-accordion__opener',
        slider: '>.slide',
        collapsible: true,
        animSpeed: 300,
        allowMultiple: true
    });

    $element.find('.c-main-navigation-item-dropdown').slideAccordion({
        slider: '>li',
        collapsible: true,
        animSpeed: 300,
        allowMultiple: true
    });
}

function initTabs($element) {
    $element.find('.js-tabs').contentTabs({
        autoHeight: false,
        animSpeed: 0,
        switchTime: 0,
        effect: 'none',
        tabLinks: 'a'
    });
}

$(document).ready(function() {
    initTabs($(document));
    initAccordion($(document));
});

function initScrollToElementButton($element) {
    $element.click(function (e) {
        e.preventDefault();

        var hash = $(e.currentTarget).data('scroll-to') || e.currentTarget.hash;

        $('html, body').animate({
            scrollTop: $(hash).offset().top - 100
        });
    });
}

function initCertificatesSections($element) {
    applyGlobalScriptsForElement($element);
    initTabs($element);

    new LazyLoad({
        elements_selector: ".c-certificates-section .lazy"
    });

    var owlSert = $element.find('.sert-slider').owlCarousel({
        startPosition: 0,
        items: 5,
        margin: 30,
        stagePadding: 0,
        smartSpeed: 450,
        slideBy: 5,
        loop: false,
        responsive: {
            0: {
                items: 1
            },
            500: {
                items: 2
            },
            800: {
                items: 3
            },
            1000: {
                items: 4
            },
            1200: {
                items: 5
            }
        }
    });
    owlSert.owlCarousel();
    $element.find('.sert-next').click(function(e) {
        e.preventDefault();
        owlSert.trigger('next.owl.carousel');
    });
    $element.find('.sert-prev').click(function(e) {
        e.preventDefault();
        owlSert.trigger('prev.owl.carousel');
    });

    var owlSert1 = $element.find('.pos-slider').owlCarousel({
        startPosition: 0,
        items: 2,
        margin: 30,
        stagePadding: 0,
        smartSpeed: 450,
        slideBy: 2,
        loop: false,
        responsive: {
            0: {
                items: 1
            },
            500: {
                items: 2
            },
            800: {
                items: 2
            },
            1000: {
                items: 2
            },
            1200: {
                items: 2
            }
        }
    });
    owlSert1.owlCarousel();
    $element.find('.pos-next').click(function(e) {
        e.preventDefault();
        owlSert1.trigger('next.owl.carousel');
    });
    $element.find('.pos-prev').click(function(e) {
        e.preventDefault();
        owlSert1.trigger('prev.owl.carousel');
    });
}

jQuery(document).ready(function($) {
    applyGlobalScriptsForElement(document.body);

    $('.js-reviews-opener, .js-reviews-close').on('click', function(e) {
        e.preventDefault();
        $('.mass-media, .offices-box, .offices-box__popup').removeClass('active');
        $(this).closest('.rating-info').toggleClass('active');
    });
    $('.js-mass-opener, .js-mass-close').on('click', function(e) {
        e.preventDefault();
        $('.rating-info, .offices-box, .offices-box__popup').removeClass('active');
        $('.mass-media').removeClass('active');
        $(this).closest('.mass-media').toggleClass('active');

    });
    $('.js-office-opener, .js-office-close').on('click', function(e) {
        e.preventDefault();
        $('.rating-info, .mass-media, .offices-box__popup').removeClass('active');
        $(this).closest('.offices-box').toggleClass('active');
    });
    $('.js-sub-office-opener, .js-sub-office-close').on('click', function(e) {
        e.preventDefault();
        $(this).closest('.offices-box__popup').toggleClass('active');
    });

    var owl = $('.promo-slider').owlCarousel({
        loop: false,
        margin: 100,
        nav: true,
        items: 1,
        dotsContainer: '.promo-box-dots',
        rewind: true,
        smartSpeed: 500,
        autoplay: false,
        stagePadding: 15,
        responsive: {
            0: {},
            800: {},
            1000: {}
        }
    });

    $('.owl-dot').click(function() {
        owl.trigger('refresh.owl.carousel');
        owl.trigger('to.owl.carousel', [$(this).index(), 500]);
    });
    $(window).bind('scroll', function() {
        if ($(window).scrollTop() > 145) {
            $('.fixed-bar').addClass('active');
        } else {
            $('.fixed-bar').removeClass('active');
            $('.fixed-bar').removeClass('drop-active');
        }
    });
    $('.top-nav .has-drop').on('click', function(e) {
        e.preventDefault();

        $('.top-nav .has-drop').each(function(index, element) {
            $(this).parent().removeClass('active');
            $('.top-nav-wrap').removeClass('top-nav-active');
        });

        $(this).parent().toggleClass('active');

        $('.top-nav-wrap').toggleClass('top-nav-active');
        $('.rating-info, .offices-box, .mass-media').removeClass('active');
    });
    $('.js-top-nav-close').on('click', function(e) {
        e.preventDefault();
        $('.top-nav-wrap').toggleClass('top-nav-active');
        $('.top-nav-wrap').find('.top-nav__item').removeClass('active');
    });
    $('[data-main-nav]').on('click', function(e) {
        e.preventDefault();
        var target = $(this).attr('data-main-nav');

        $('.js-mobile-navigation-drop[data-navigation-id="' + target + '"]')
            .addClass('active');
    });
    $('[data-link]').on('click', function(e) {
        e.preventDefault();
        var val = $(this).attr('data-link');
        var tagHtml = $(this).html();
        console.log(tagHtml);
        $('.mobile-nav-second').addClass('active');
        $('.mobile-nav-second .btn-back').html(tagHtml);
    });
    $('.mobile-nav-first .btn-back').on('click', function(e) {
        e.preventDefault();
        $('.mobile-nav-first').removeClass('active');
    });
    $('.mobile-nav-second .btn-back').on('click', function(e) {
        e.preventDefault();
        $('.mobile-nav-second').removeClass('active');
    });
    $('.main-nav-opener').on('click', function(e) {
        e.preventDefault();
        $('.mobile-nav-wrap').toggleClass('active');
    });
    $('.fixed-menu-opener').click(function(e){
        e.preventDefault();
        $('.mobile-nav-wrap').toggleClass('active');

        $('body, html').animate({
            scrollTop: 0
        }, 100);
    });
    /*$('.fixed-menu-opener, .js-top-fixed-close').on('click', function(e) {
        e.preventDefault();
        $(this).closest('.fixed-bar').toggleClass('drop-active');
    });*/
    $('.main-nav-opener-fixed').on('click', function(e) {
        e.preventDefault();
        $(this).closest('.fixed-bar').toggleClass('drop-active');
    });

    {
        var articleSlider = $('.articles-slider.owl-carousel').owlCarousel({
            startPosition: 0,
            items: 3,
            margin: 30,
            stagePadding: 0,
            smartSpeed: 450,
            loop: false,
            dots: false,
            responsive: {
                0: {
                    items: 1
                },
                800: {
                    items: 2
                },
                1000: {
                    items: 3
                },
                1200: {
                    items: 4
                }
            }
        });

        $('.articles-next').click(function(e) {
            e.preventDefault();
            articleSlider.trigger('next.owl.carousel');
        });
        $('.articles-prev').click(function(e) {
            e.preventDefault();
            articleSlider.trigger('prev.owl.carousel');
        });
    }











    var owlRev2 = $('.rev-slider2').owlCarousel({
        startPosition: 0,
        items: 3,
        margin: 30,
        stagePadding: 0,
        smartSpeed: 450,
        slideBy: 3,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            1000: {
                items: 3
            }
        }
    });

    owlRev2.owlCarousel();
    $('.rev-next2').click(function(e) {
        e.preventDefault();
        owlRev2.trigger('next.owl.carousel');
    });
    $('.rev-prev2').click(function(e) {
        e.preventDefault();
        owlRev2.trigger('prev.owl.carousel');
    });



    $('.js-category-list-opener').click(function(e) {
       e.preventDefault();
        $(this).hide();
        $('.category-list-hidden').addClass('active');
    });

    var suggestServiceOptions = {
        url: function(phrase) {
            return "/api/web/search/services?q=" + phrase;
        },

        requestDelay: 250,

        getValue: "name",

        template: {
            type: "links",
            fields: {
                link: "url"
            }
        },

        list: {
            maxNumberOfElements: 10
        }
    };

    $(".js-search-service-input").easyAutocomplete(suggestServiceOptions);
});

function showCustomAlertBox(message) {
    jQuery('#user-alert-box').html(message);

    jQuery('.thank_form,.over_all').show();

    setTimeout(function(){
        jQuery('.thank_form,.over_all').hide();
    },3000);
}

jQuery(document).ready(function(){
    $(".connect__button").on("click", function() {
        $(this).toggleClass("x-active");
    });

    jQuery('.copy_contact').click(function(){
        var succeed = copyToClipboard(jQuery('#contacts-to-clipboard').get(0));
        var msg;

        if (!succeed) {
            msg = "Автоматическое копирование не поддерживается вашим браузером"
        } else {
            msg = "Контакты скопированы в буфер обмена."
        }
        jQuery('.copy_message').html(msg);

        showCustomAlertBox(msg);
    });


    jQuery('.over_all,.thank_close').click(function(){
        jQuery('.thank_form,.over_all').hide();
    });

    jQuery('.zones-list span[data-id]').click(function(){
        jQuery('.popup_offices-list').removeClass('active');
        jQuery('.popup_offices-list[data-id="'+jQuery(this).attr('data-id')+'"]').addClass('active');
    });
    jQuery('.m_zones-list span[data-id]').click(function(){
        jQuery('.m_offices-list').removeClass('active');
        jQuery('.m_offices-list[data-id="'+jQuery(this).attr('data-id')+'"]').addClass('active');
    });


    jQuery('.first-level span[data-id]').click(function(){
        jQuery('.first-level a').removeClass('active');
        jQuery(this).parent().addClass('active');
        jQuery('.popup_service_list').removeClass('active');

        var dataId = jQuery(this).attr('data-id');

        jQuery('.popup_service_list[data-id="'+ dataId+'"]').addClass('active');
        jQuery('.popup_service_title').html(jQuery(this).html());

        jQuery('.js-specialization-menu__specialist-chunk').css('display', 'none');
        jQuery('.js-specialization-menu__specialist-chunk[data-id="' + dataId + '"]').css('display', 'block');
    });

    jQuery('.bottom_list a').click(function(e){
        e.preventDefault();
    });
    jQuery('.bottom_list span[data-id]').click(function(){
        console.log(222);
        jQuery('.bottom_list a').removeClass('active');
        jQuery(this).parent().parent().addClass('active');
        jQuery('.bottom_services .popup_service_list').removeClass('active');
        jQuery('.bottom_services .popup_service_list[data-id="'+jQuery(this).attr('data-id')+'"]').addClass('active');
        jQuery('.bottom_services .popup_service_title').html(jQuery(this).html());
    });

});
function copyToClipboard(elem) {
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }

    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}
